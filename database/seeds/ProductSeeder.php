<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $string = file_get_contents(__DIR__."/json/products.json");

        $data = [];

        foreach (json_decode($string) as $item){
            $product = new \App\Product();

            $product->old_id = $item->id;
            $product->photo = strlen($item->photo) > 5 ? 'assets/uploads/'.$item->photo : null;
            $product->draw = strlen($item->draw) > 5 ? 'assets/uploads/'.$item->draw : null;
            $product->alias = $item->alias.'-'.$item->id;
            $product->name = $item->name;
            $product->meta_title = $item->meta_title;
            $product->meta_description = $item->meta_description;
            $product->meta_keywords = $item->meta_keywords;
            $product->header = $item->header;
            $product->content = $item->description;

            $product->sku = strlen($item->sku) > 2 ? $item->sku : null;
            $product->price = $item->price;
            $product->hit = $item->hit;
            $product->stock = $item->stock;
            $product->warehouse = $item->warehouse;
            $product->under_order = $item->underorder;
            $product->active = $item->active;

            $product->save();

            foreach (explode(',', $item->categories) as $cat){
                $category = \App\Category::where('old_id', $cat)->get();
                $product->category()->attach($category);
            }


        }

//        $product->name = 'РК 50-7-418';
//        $product->alias = 'RK50-7-4188';
//        $product->sku = 'RK50-7-4181';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->stock = 1;
//        $product->under_order = 1;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-418';
//        $product->meta_description = 'РК 50-7-418';
//        $product->meta_keywords = 'РК 50-7-418';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-417';
//        $product->alias = 'RK50-7-4177';
//        $product->sku = 'RK50-7-4172';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->topseller = 1;
//        $product->stock = 0;
//        $product->under_order = 0;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-417';
//        $product->meta_description = 'РК 50-7-417';
//        $product->meta_keywords = 'РК 50-7-417';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-418';
//        $product->alias = 'RK50-7-4186';
//        $product->sku = 'RK50-7-4183';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->topseller = 1;
//        $product->stock = 1;
//        $product->under_order = 1;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-418';
//        $product->meta_description = 'РК 50-7-418';
//        $product->meta_keywords = 'РК 50-7-418';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-417';
//        $product->alias = 'RK50-7-4175';
//        $product->sku = 'RK50-7-4174';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->topseller = 1;
//        $product->stock = 0;
//        $product->under_order = 0;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-417';
//        $product->meta_description = 'РК 50-7-417';
//        $product->meta_keywords = 'РК 50-7-417';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-418';
//        $product->alias = 'RK50-7-4184';
//        $product->sku = 'RK50-7-4185';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->stock = 1;
//        $product->under_order = 1;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-418';
//        $product->meta_description = 'РК 50-7-418';
//        $product->meta_keywords = 'РК 50-7-418';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-417';
//        $product->alias = 'RK50-7-4173';
//        $product->sku = 'RK50-7-4176';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->stock = 0;
//        $product->under_order = 0;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-417';
//        $product->meta_description = 'РК 50-7-417';
//        $product->meta_keywords = 'РК 50-7-417';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-418';
//        $product->alias = 'RK50-7-4182';
//        $product->sku = 'RK50-7-4187';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->stock = 1;
//        $product->under_order = 1;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-418';
//        $product->meta_description = 'РК 50-7-418';
//        $product->meta_keywords = 'РК 50-7-418';
//        $product->save();
//        $product->category()->attach($category);
//
//        $product = new \App\Product();
//        $category = \App\Category::find(17);
//
//        $product->name = 'РК 50-7-417';
//        $product->alias = 'RK50-7-4171';
//        $product->sku = 'RK50-7-4178';
//        $product->price = '631';
//        $product->warehouse = 612;
//        $product->hit = 1;
//        $product->stock = 0;
//        $product->under_order = 0;
//        $product->active = 1;
//        $product->short_description = 'test test test test test test test test test test test test test test';
//
//        $product->meta_title = 'РК 50-7-417';
//        $product->meta_description = 'РК 50-7-417';
//        $product->meta_keywords = 'РК 50-7-417';
//        $product->save();
//        $product->category()->attach($category);
    }
}
