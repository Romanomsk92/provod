<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new \App\Setting();
        $setting->key = 'product_meta_title';
        $setting->name = 'Маска для тайтла товара';
        $setting->value = 'Купить {product_name} по цене {product_price} | Сопротивление {product_resistance} Ом, диаметр {product_diameter} мм, ГОСТ/ТУ {product_gost}';
        $setting->save();

        $setting = new \App\Setting();
        $setting->key = 'product_meta_description';
        $setting->name = 'Маска для дескрипшена товара';
        $setting->value = 'Предприятие Паритет ☎ +7 (843) 557-48-49 предлагает купить {product_name} сопротивление {product_resistance} Ом, диаметр {product_diameter} мм, ГОСТ/ТУ {product_gost} по низким ценам';
        $setting->save();

        $setting = new \App\Setting();
        $setting->key = 'category_meta_title';
        $setting->name = 'Маска для тайтла категории';
        $setting->value = '{category_name} купить по низким ценам ☎ +7 (843) 557-48-49';
        $setting->save();

        $setting = new \App\Setting();
        $setting->key = 'category_meta_description';
        $setting->name = 'Маска для дескрипшена категории';
        $setting->value = 'Предприятие Паритет предлагает купить {category_name категории} по низким ценам';
        $setting->save();
    }
}
