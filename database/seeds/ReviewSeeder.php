<?php

use App\Product;
use App\Review;
use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $review = new Review();
        $product = Product::find(1);

        $review->name = 'test';
        $review->rating = 5;
        $review->email = 'test@test.test';
        $review->comment = 'testtest';
        $review->save();

        $review->product()->attach($product);
    }
}
