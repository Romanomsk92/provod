<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new \App\Page();
        $page->name = 'Гарантии';
        $page->alias = 'garantii';
        $page->meta_title = 'Гарантии';
        $page->meta_description = 'Гарантии';
        $page->meta_keywords = 'Гарантии';
        $page->header = 'Гарантии';
        $page->content = '<h2>
            Гарантия качества!</h2>
        <p style="text-align: justify;">
            Строго соблюдается соответствие изделий всем современным стандартам и требованиям.</p>
        <h2>
            Гарантия обмена!</h2>
        <p style="text-align: justify;">
            В случае поставки продукции ненадлежащего качества, мы обязуемся произвести замену товара.</p>
        <p style="text-align: justify;">
            Вы получаете 100% заводскую гарантию на замену в случае заводского брака.</p>
        ';
        $page->save();

        $page = new \App\Page();
        $page->name = 'Доставка и оплата';
        $page->alias = 'dostavka-i-oplata';
        $page->meta_title = 'Доставка и оплата';
        $page->meta_description = 'Доставка и оплата';
        $page->meta_keywords = 'Доставка и оплата';
        $page->header = 'Доставка и оплата';
        $page->content = '<p style="text-align: justify;">
            <span style="font-size:18px;">Доставка автомобильным транспортом</span></p>
        <p style="text-align: justify;">
            ООО &quot;Предприятие &quot;ПАРИТЕТ&quot; осуществляет доставку до терминала транспортной компании &quot;Деловые Линии&quot; в г.Казань</p>
        <p style="text-align: justify;">
            Вы можете заказать забор груза с нашего склада любой, удобной для Вас, транспортной компанией.</p>
        <p style="text-align: justify;">
            &nbsp;</p>
        <p style="text-align: center;">
            <img alt="" height="200" src="/admin/uploads/image/1.jpg" width="362" /> <img alt="" height="200" src="/admin/uploads/image/2.jpg" width="300" /></p>
        ';
        $page->save();

        $page = new \App\Page();
        $page->name = 'О компании';
        $page->alias = 'o-kompanii';
        $page->template = 'aboutus';
        $page->meta_title = 'О компании';
        $page->meta_description = 'О компании';
        $page->meta_keywords = 'О компании';
        $page->header = 'О компании ООО "Предприятие "ПАРИТЕТ"';
        $page->content = '<p style="text-align: justify;">
            ООО &quot;Предприятие &quot;ПАРИТЕТ&quot; занимается торговлей кабельно-проводниковой продукцией. Мы предлагаем широкий выбор кабельно-проводниковой продукции: отечественный импортный кабель, силовой кабель, провода, кабель коаксиальный, контрольный кабель, кабель управления.</p>
        <p style="text-align: justify;">
            Мы постоянно расширяем ассортимент, проводя маркетинговые исследования и оперативно реагируя на изменения потребностей клиентов. Нашими партнерами являются оптовые фирмы, строительные организации.</p>
        <p style="text-align: justify;">
            Мы осуществляем поставки нашей продукции заказчикам по всей России. Оцените наш дружелюбный сервис, оперативное обслуживание и индивидуальный подход к каждому клиенту.</p>
        ';
        $page->save();

        $page = new \App\Page();
        $page->name = 'Контакты';
        $page->alias = 'kontakti';
        $page->template = 'contacts';
        $page->meta_title = 'Контакты';
        $page->meta_description = 'Контакты';
        $page->meta_keywords = 'Контакты';
        $page->header = 'Контакты';
        $page->content = '<p style="text-align: center;">
            <span style="font-size:24px;"><strong>Карта предприятия <a href="/admin/uploads/file/карта Предприятие Паритет_(3).docx">скачать</a></strong></span></p>
        ';
        $page->save();

        $page = new \App\Page();
        $page->name = 'Главная';
        $page->alias = '/';
        $page->template = '';
        $page->meta_title = 'Контакты';
        $page->meta_description = 'Контакты';
        $page->meta_keywords = 'Контакты';
        $page->header = 'Контакты';
        $page->content = '';
        $page->save();
    }
}
