<?php

use Illuminate\Database\Seeder;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testimonial = new \App\Testimonial();

        $testimonial->name = 'Test 1';
        $testimonial->position = 'Work 1';
        $testimonial->text = 'Lorem Ipsum is simply dummy text of tled it te spec. It has survived not only five centuries';
        $testimonial->rating = 1;
        $testimonial->save();

        $testimonial = new \App\Testimonial();

        $testimonial->name = 'Test 2';
        $testimonial->position = 'Work 2';
        $testimonial->text = 'Lorem Ipsum is simply dummy text of tled it te spec. It has survived not only five centuries';
        $testimonial->rating = 2;
        $testimonial->save();

        $testimonial = new \App\Testimonial();

        $testimonial->name = 'Test 3';
        $testimonial->position = 'Work 3';
        $testimonial->text = 'Lorem Ipsum is simply dummy text of tled it te spec. It has survived not only five centuries';
        $testimonial->rating = 3;
        $testimonial->save();

        $testimonial = new \App\Testimonial();

        $testimonial->name = 'Test 4';
        $testimonial->position = 'Work 4';
        $testimonial->text = 'Lorem Ipsum is simply dummy text of tled it te spec. It has survived not only five centuries';
        $testimonial->rating = 4;
        $testimonial->save();
    }
}
