<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $category = new \App\Category();
//
//        $category->name = 'Кабели радиочастотные';
//        $category->alias = 'kabeli-radiochastotnie';
//        $category->meta_title = 'Кабели радиочастотные коаксиальные | Купить радиокабели по низким ценам от производителя Паритет';
//        $category->meta_description = 'Производитель кабельной продукции Паритет предлагает купить радиокабели по оптимально низким ценам';
//        $category->meta_keywords = 'Кабели радиочастотные';
//        $category->header = 'Кабели радиочастотные';
//        $category->save();
//
//        $category = new \App\Category();
//
//        $category->name = 'Кабель РК 50';
//        $category->parent_id = 1;
//        $category->alias = 'kabel-rk-50-';
//        $category->meta_title = 'РК 50 коаксиальные радиочастотные кабели купить по низким ценам за метр от производителя Паритет';
//        $category->meta_description = 'Коаксиальные радиочастотные кабели (РК) —, используются в антенно-фидерных устройствах, радиопередатчиках, радио- и видеоприемниках, спутниковой и космической связи, в радиоизмерительных и радиоэлектронных приборах, вычислительной технике и пр.';
//        $category->meta_keywords = 'Кабель РК 50 ';
//        $category->header = 'Кабель РК 50 ';
//        $category->content_before = 'Коаксиальные радиочастотные кабели (РК) —, используются в антенно-фидерных устройствах, радиопередатчиках, радио- и видеоприемниках, спутниковой и космической связи, в радиоизмерительных и радиоэлектронных приборах, вычислительной технике и пр.';
//        $category->save();

        $string = file_get_contents(__DIR__."/json/cat.json");

        $data = [];

        foreach (json_decode($string) as $cat){
            if($cat->parent_id > 0){
                continue;
            }
            $category = $this->insert($cat, 0);
            $category->save();
        }

        foreach (json_decode($string) as $cat){
            if($cat->parent_id == 0){
                continue;
            }

            $check = \App\Category::where('old_id', $cat->parent_id)->first();

            if($check){
                $category = $this->insert($cat, $check->id);
                $category->save();
            }else{
                $data[] = $cat;
            }
        }

        foreach ($data as $cat){
            $check = \App\Category::where('old_id', $cat->parent_id)->first();
            $category = $this->insert($cat, $check->id);
            $category->save();
        }

    }

    function insert($cat, $parent){
        $category = new \App\Category();

        $category->old_id = $cat->id;
        $category->name = $cat->name;
        $category->parent_id = $parent;
        $category->alias = $cat->alias.'-'.$cat->id;
        $category->meta_title = $cat->meta_title;
        $category->meta_description = $cat->meta_description;
        $category->meta_keywords = $cat->meta_keywords;
        $category->header = $cat->header;
        $category->photo = strlen($cat->photo) > 5 ? 'assets/uploads/'.$cat->photo : null;
        $category->content_before = $cat->content_before;
        $category->content_after = $cat->content_after;

        return $category;
    }

}
