<?php

use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slide = new \App\Slider();
        $slide->name = 'test1';
        $slide->image = 'images/uploads/ec4f7b66c24bb27e5220563a26c501fd.png';
        $slide->link = '/test';
        $slide->link_text = 'Перейти';
        $slide->active = 1;
        $slide->line1 = 'test';
        $slide->line2 = 'test';
        $slide->line3 = 'test';
        $slide->line4 = 'test';
        $slide->line5 = 'test';
        $slide->position = 0;

        $slide->save();

        $slide = new \App\Slider();
        $slide->name = 'test1';
        $slide->image = 'images/uploads/ec4f7b66c24bb27e5220563a26c501fd.png';
        $slide->link = '/test';
        $slide->link_text = 'Перейти';
        $slide->active = 1;
        $slide->line1 = 'test2';
        $slide->line2 = 'test3';
        $slide->line3 = 'test4';
        $slide->line4 = 'test5';
        $slide->line5 = 'test6';
        $slide->position = 1;

        $slide->save();

    }
}
