<?php

use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $attribute = new \App\Attribute();
        $attribute->old_id = 189;
        $attribute->name = 'Маркировка';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 191;
        $attribute->name = 'Длина';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'м';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 192;
        $attribute->name = 'Ширина';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'мм';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 195;
        $attribute->name = 'Сопротивление';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'Ом';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 194;
        $attribute->name = 'Вес';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'кг/км';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 199;
        $attribute->name = 'Минимальная рабочая температура';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = '°С';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 197;
        $attribute->name = 'Максимальная рабочая температура';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = '°С';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 198;
        $attribute->name = 'Наружный диаметр кабеля';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'мм';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 200;
        $attribute->name = 'Рабочее напряжение';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'В';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 201;
        $attribute->name = 'Количество жил';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'шт';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 202;
        $attribute->name = 'Номинальное сечение';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'мм²';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 203;
        $attribute->name = 'Номинальный диаметр';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->measure = 'мм²';
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 193;
        $attribute->name = 'Стандарт';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->save();

        $attribute = new \App\Attribute();
        $attribute->old_id = 204;
        $attribute->name = 'Категория';
        $attribute->alias = $this->str2url($attribute->name);
        $attribute->save();



        $string = file_get_contents(__DIR__."/json/attrubute.json");

        $data = [];

        foreach (json_decode($string) as $item){
           $product = \App\Product::where('old_id', $item->product)->first();
           $attribute = \App\Attribute::where('old_id', $item->arrtibute)->first();

           if($attribute && $product){
               $product->attributes()->attach($attribute, ['value'=>$item->value]);
           }
        }

    }

    public function rus2translit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }
    public function str2url($str) {
        // переводим в транслит
        $str = $this->rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }
}
