<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->string('name')->charset('utf8');
            $table->string('alias')->unique()->charset('utf8');
            $table->string('sku')->nullable();
            $table->integer('price')->nullable();
            $table->integer('hit')->nullable();
            $table->integer('topseller')->nullable();
            $table->integer('stock')->nullable();
            $table->integer('warehouse')->nullable();
            $table->integer('under_order')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('header')->nullable();
            $table->text('short_description')->nullable();
            $table->text('content')->nullable()->charset('utf8');
            $table->string('photo')->nullable();
            $table->string('draw')->nullable();
            $table->integer('active')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
