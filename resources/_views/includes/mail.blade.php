<div class="well col-sm-8">

    @foreach($post as $key=>$item)

        @switch($key)

            @case('your-name')

            <p>Имя: {{ $item }}</p>
 
            @break

            @case('name')

            <p>Имя: {{ $item }}</p>
 
            @break

            @case('email')

            <p>Email: {{ $item }}</p>

            @break

            @case('Email')

            <p>Email: {{ $item }}</p>

            @break

            @case('your-phone')

            <p>Телефон: {{ $item }}</p>

            @break

            @case('phone')

            <p>Телефон: {{ $item }}</p>
 
            @break

            @case('delivery')

            <p>Доставка: {{ $item }}</p>
 
            @break

            @case('comment')

            <p>Сообщение: {{ $item }}</p>
 
            @break

            @case('message')

            @foreach(explode('|', $item) as $i)
                <p>{{ $i }}</p>
            @endforeach

            @break

        @endswitch

    @endforeach

    {{ $feedback }}

</div>
