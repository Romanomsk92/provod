<div class="top_bar">
    <div class="container">
        <div class="row">
            <div class="col d-md-flex flex-row">
                <div class="top_bar_contact_item d-flex">
                    <div class="warehouse">
                        Склад: <a href="/contacts">г. Казань ул. Восстания, д. 104 </a>
                    </div>
                    <div class="top-bar_email ml-4">
                        <a href="mailto:info@pro-vod.ru">info@pro-vod.ru</a>
                    </div>
                </div>
                <div class="top_bar_content ml-auto">
                    <div>ПН-ПТ: 9.00 - 17.00, СБ, ВС: выходной</div>

                </div>
            </div>
        </div>
    </div>
</div><!-- top_bar end-->
