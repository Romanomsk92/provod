<!--header start-->
<header id="masthead" class="header ttm-header-style-01">
    <!-- top_bar -->
    @include('frontend.parts.topbar')
    <!-- header_main -->
    @include('frontend.parts.headermain')
    <!-- site-header-menu -->
    <div id="site-header-menu" class="site-header-menu ttm-bgcolor-white clearfix">
        <div class="site-header-menu-inner stickable-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main_nav_content d-flex flex-row">
                            <div class="cat_menu_container">
                                <a href="#" class="cat_menu d-flex flex-row align-items-center">
                                    <div class="cat_icon"><i class="fa fa-bars"></i></div>
                                    <div class="cat_text"><h4>Каталог</h4></div>
                                </a>

                                <ul class="cat_menu_list menu-vertical">
                                    <li><a href="#" class="close-side"><i class="fa fa-times"></i></a></li>

                                    @include('frontend.parts.categories')
                                </ul>
                            </div>

                            <!--site-navigation -->
                            <div id="site-navigation" class="site-navigation">
                                <div class="btn-show-menu-mobile menubar menubar--squeeze">
                                    <span class="menubar-box">
                                        <span class="menubar-inner"></span>
                                    </span>
                                </div>
                                <!-- menu -->
                                <nav class="menu menu-mobile" id="menu">
                                    <ul class="nav">
                                        @foreach($pages as $page)
                                            <li><a href="/{{ $page->alias }}">{{ $page->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </nav>
                            </div><!-- site-navigation end-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- site-header-menu end -->
</header>
<!--header end-->
