<div class="cart dropdown">
    <div class="dropdown_link d-flex flex-row align-items-center justify-content-end" data-toggle="dropdown">
        <div class="cart_icon">
            <i class="fa fa-shopping-cart"></i>
            <div class="cart_count">{{ $cart_quantity }}</div>
        </div>
        <div class="cart_content">
            <div class="cart_text"><a href="#">Корзина</a></div>
            <div class="cart_price">{{ number_format($cart_summ, '0', '.', ' ') }} ₽</div>
        </div>
    </div>
    @if(isset($cart))
        <aside class="widget_shopping_cart dropdown_content">
            <ul class="cart-list">
                @foreach($cart as $item)
                    <li>
                        <a href="#" class="photo"><img src="{{ asset($item['photo']) }}" class="cart-thumb" alt="" /></a>
                        <h6><a href="{{ $item['url'] }}">{{ $item['name'] }}</a></h6>
                        <p>{{ $item['quantity'] }} шт - <span class="price">{{ $item['quantity'] * $item['price'] }} ₽</span></p>
                    </li>
                @endforeach


                <li class="total">
                    <span class="pull-right"><strong>Итого</strong>: {{ $cart_summ }} ₽</span>
                    <a href="/cart" class="btn btn-default btn-cart">Корзина</a>
                </li>
            </ul>
        </aside>
    @endif
</div>
