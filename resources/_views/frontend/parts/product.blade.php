<div class="product col-md-3 col-sm-6 col-xs-12">
    <div class="product-box">
        <!-- product-box-inner -->
        <div class="product-box-inner">
            <div class="product-image-box">
                @if($item->stock == 1)
                    <div class="onsale">Распродажа!</div>
                @endif
                <img class="img-fluid pro-image-front" src="{{ asset($item->photo) }}" alt="">
                <img class="img-fluid pro-image-back" src="{{ asset($item->photo) }}" alt="">
            </div>
            <div class="product-btn-links-wrapper">
                <div class="product-btn">
                    <a href="#"
                       class="add-to-cart-btn tooltip-top"
                       data-id="{{ $item->id }}"
                       data-tooltip="Добавить в корзину">
                            <i class="ti ti-shopping-cart"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="product-content-box">
            <a class="product-title" href="{{ $item->Url }}">
                <h2>{{ $item->name }}</h2>
            </a>
{{--            <div class="star-ratings">--}}
{{--                <ul class="rating">--}}
{{--                    @for($i = 0; $i < $item->rating; $i++)--}}
{{--                        <li class="fa fa-star"></li>--}}
{{--                    @endfor--}}
{{--                    @for($i = 0; $i < 5 - $item->rating; $i++)--}}
{{--                        <li class="fa fa-star-o"></li>--}}
{{--                    @endfor--}}
{{--                </ul>--}}
{{--            </div>--}}
            <span class="price">
                <ins>
                    <span class="product-Price-amount">
                        {{ $item->price }}<span
                            class="product-Price-currencySymbol">₽</span>
                    </span>
                </ins>
            </span>
        </div>
    </div>
</div>
