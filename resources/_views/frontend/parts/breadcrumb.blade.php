<!-- page-title -->
<div class="ttm-page-title-row">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="page-title-heading">
                        <h1 class="title">@if(isset($header)) {{ $header }} @endif</h1>
                    </div>

                    <div class="breadcrumb-wrapper">
                        <span class="mr-1"><i class="ti ti-home"></i></span>
                        <a title="Homepage" href="/">Главная</a>
                        @foreach($breadcrumb as $key => $item)

                            <span class="ttm-bread-sep">&nbsp;/&nbsp;</span>

                            @if(isset($item['url']) && $key != count($breadcrumb) - 1)
                                <a href="{{ $item['url'] }}"><span class="ttm-textcolor-skincolor">{{ $item['name'] }}</span></a>
                            @else
                                <span class="ttm-textcolor-skincolor">{{ $item['name'] }}</span>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- page-title end-->
