<div id="rev_slider_5_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container slide-overlay" data-alias="classic4export" data-source="gallery">
    <!-- START REVOLUTION SLIDER -->
    <div id="rev_slider_5_1" class="rev_slider fullwidthabanner" data-version="5.4.8">
        <ul>
            @foreach($slides as $item)
                @php
                    if($item->position == 0){
                        $position = 'left';
                    }else{
                        $position = 'right';
                    }
                @endphp
                <li data-index="rs-{{ $item->id }}" data-transition="fade" data-slotamount="1" data-easein="default" data-easeout="default" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                    <!-- MAIN IMAGE -->
                    <img src="{{ asset($item->image) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

                    @if($item->line1)
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme" id="slide-{{ $item->id }}-layer-1" data-x="['{{ $position }}','{{ $position }}','{{ $position }}','center']" data-hoffset="['50','40','-520','-520']"
                         data-y="['top','top','top','top']" data-voffset="['103','103','63','63']"

                         data-fontsize="['23','23','23','23']"
                         data-lineheight="['30','30','30','30']"
                         data-color="['rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-transform_idle="o:1;"
                         data-transform_in="x:[100%];y:0px;s:1500;e:Power3.easeOut;"
                         data-transform_out="x:auto;y:auto;opacity:0;s:1500;e:Power4.easeIn;"
                         data-mask_in="x:[-150%];y:0px;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:inherit;e:inherit;"
                         data-start="600"

                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-type="text"
                         data-responsive_offset="on"> {{ $item->line1 }}
                    </div>
                    @endif

                    @if($item->line2)
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption rev-shape ttm-bgcolor-skincolor tp-resizeme" id="slide-{{ $item->id }}-layer-2" data-x="['{{ $position }}','{{ $position }}','{{ $position }}','center']"
                         data-hoffset="['50','40','30','0']" data-y="['top','top','top','middle']" data-voffset="['151','151','80','-100']"

                         data-fontsize="['16','16','14','13']"
                         data-lineheight="['20','20','20','12']"
                         data-fontweight="['400','400','400','400']"
                         data-color="['rgb(255, 255, 255)','rgb(255, 255, 255)','rgb(255, 255, 255)','rgb(242, 244, 248)']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-transform_idle="o:1;"
                         data-transform_in="x:[100%];y:0px;s:1500;e:Power3.easeOut;"
                         data-transform_out="x:auto;y:auto;opacity:0;s:1500;e:Power4.easeIn;"
                         data-mask_in="x:[-150%];y:0px;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:inherit;e:inherit;"
                         data-start="800"

                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[13,13,10,10]"
                         data-paddingright="[22,22,22,10]"
                         data-paddingbottom="[11,11,10,10]"
                         data-paddingleft="[22,22,22,10]"
                         data-type="text"
                         data-responsive_offset="on">{{ $item->line2 }}
                    </div>
                    @endif

                    @if($item->line3)
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme" id="slide-{{ $item->id }}-layer-3" data-x="['{{ $position }}','{{ $position }}','{{ $position }}','center']" data-hoffset="['50','40','30','0']"
                         data-y="['top','top','top','middle']" data-voffset="['219','219','140','-45']"

                         data-fontsize="['48','48','42','42']"
                         data-lineheight="['52','52','48','48']"
                         data-fontweight="['600','600','600','600']"
                         data-color="['rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-transform_idle="o:1;"
                         data-transform_in="x:[100%];y:0px;s:1500;e:Power3.easeOut;"
                         data-transform_out="x:auto;y:auto;opacity:0;s:1500;e:Power4.easeIn;"
                         data-mask_in="x:[-150%];y:0px;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:inherit;e:inherit;"
                         data-start="1000"

                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-type="text"
                         data-responsive_offset="on">{{ $item->line3 }}
                    </div>
                    @endif

                    @if($item->line4)
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption tp-resizeme" id="slide-{{ $item->id }}-layer-4" data-x="['{{ $position }}','{{ $position }}','{{ $position }}','center']" data-hoffset="['50','40','30','0']"
                         data-y="['top','top','top','middle']" data-voffset="['286','286','199','0']"

                         data-fontsize="['48','48','42','42']"
                         data-lineheight="['52','52','48','48']"
                         data-fontweight="['600','600','600','600']"
                         data-color="['rgba(255, 210, 0,1)','rgba(255, 210, 0,1)','rgba(255, 210, 0,1)','rgba(255, 210, 0,1)']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-transform_idle="o:1;"
                         data-transform_in="x:[100%];y:0px;s:1500;e:Power3.easeOut;"
                         data-transform_out="x:auto;y:auto;opacity:0;s:1500;e:Power4.easeIn;"
                         data-mask_in="x:[-150%];y:0px;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:inherit;e:inherit;"
                         data-start="1200"

                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-type="text"
                         data-responsive_offset="on">{{ $item->line4 }}
                    </div>
                    @endif

                    @if($item->line5)
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption tp-resizeme" id="slide-{{ $item->id }}-layer-5" data-x="['{{ $position }}','{{ $position }}','{{ $position }}','center']" data-hoffset="['50','40','30','0']"
                         data-y="['top','top','top','middle']" data-voffset="['352','352','255','45']"

                         data-fontsize="['48','48','42','42']"
                         data-lineheight="['52','52','48','48']"
                         data-fontweight="['600','600','600','600']"
                         data-color="['rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-transform_idle="o:1;"
                         data-transform_in="x:[100%];y:0px;s:1500;e:Power3.easeOut;"
                         data-transform_out="x:auto;y:auto;opacity:0;s:1500;e:Power4.easeIn;"
                         data-mask_in="x:[-150%];y:0px;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:inherit;e:inherit;"
                         data-start="1400"

                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-type="text"
                         data-responsive_offset="on">{{ $item->line5 }}
                    </div>
                    @endif

                    @if($item->link)
                    <a class="tp-caption rev-button ttm-bgcolor-highlight" href="{{ $item->link }}" target="_self" id="slide-{{ $item->id }}-layer-6" data-x="['{{ $position }}','{{ $position }}','{{ $position }}','center']" data-hoffset="['50','40','30','0']" data-y="['top','top','top','middle']" data-voffset="['352','352','255','45']"

                       data-fontsize="['15','15','14','12']"
                       data-lineheight="['20','20','16','15']"
                       data-fontweight="['600','600','600','600']"
                       data-color="['rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)','rgba(10, 28, 55,1)']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-transform_idle="o:1;"
                       data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:500;e:Power4.easeInOut;"
                       data-style_hover="c:rgb(255, 255, 255);bg:rgb(0, 11, 28);"
                       data-transform_in="x:[100%];y:0px;s:1500;e:Power3.easeOut;"
                       data-transform_out="x:auto;y:auto;opacity:0;s:1500;e:Power4.easeIn;"
                       data-mask_in="x:[-150%];y:0px;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:inherit;e:inherit;"
                       data-start="1600"

                       data-textAlign="['inherit','inherit','inherit','inherit']"
                       data-paddingtop="[15,15,14,14]"
                       data-paddingright="[30,30,22,22]"
                       data-paddingbottom="[14,14,12,12]"
                       data-paddingleft="[30,30,22,22]"
                       data-type="button"
                       data-responsive_offset="on" >
                        @if($item->link_text) {{ $item->link_text }} @else Перейти @endif
                            <span class="ml-2"><i class="themifyicon ti-angle-right"></i></span>
                    </a>
                    @endif

                </li>
            @endforeach

        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
</div>
