<section class="featured-product-section clearfix">
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                <!-- section-title -->
                <div class="section-title title-style-center_text style2">
                    <div class="title-header">
                        <h5>Каталог</h5>
                        <h2 class="title">Категории товаров</h2>
                    </div>
                </div><!-- section-title end -->
            </div>
        </div><!-- row end -->
        <div class="products row">

            @foreach($categories as $category)
                <!-- product -->
                    <div class="product col-md-3 col-sm-6 col-xs-12">
                        <div class="product-box">
                            <!-- product-box-inner -->
                            <div class="product-box-inner">
                                <div class="product-image-box">
                                    <img class="img-fluid pro-image-front" src="{{ asset($category->photo) }}" alt="">
                                    <img class="img-fluid pro-image-back" src="{{ asset($category->photo) }}" alt="">
                                </div>
                            </div><!-- product-box-inner end -->
                            <div class="product-content-box">
                                <a class="product-title" href="{{ $category->Url }}">
                                    <h2>{{ $category->name }}</h2>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- product end -->
            @endforeach

        </div>
    </div>
</section>
