<section class="row-top-section clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="mt_50 res-991-mt-0">
                    <div class="row no-gutters">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <!-- featured-icon-box -->
                            <div class="featured-icon-box style1 ttm-bgcolor-grey">
                                <div class="featured-icon">
                                    <div class="ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                        <i class="themifyicon ti-truck"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    {{--                                            <div class="featured-title">--}}
                                    {{--                                                <h5>Бесплатная доставка</h5>--}}
                                    {{--                                            </div>--}}
                                    <div class="featured-desc">
                                        <p>Доставка собственным автотранспортом*</p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <!-- featured-icon-box -->
                            <div class="featured-icon-box style1 ttm-bgcolor-highlight">
                                <div class="featured-icon">
                                    <div class="ttm-icon ttm-icon_element-color-darkgrey ttm-icon_element-size-md">
                                        <i class="themifyicon ti-wallet"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    {{--                                            <div class="featured-title">--}}
                                    {{--                                                <h5>100% Возврат денег</h5>--}}
                                    {{--                                            </div>--}}
                                    <div class="featured-desc">
                                        <p>Оплата картами, наличными, банковским переводом*</p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <!-- featured-icon-box -->
                            <div class="featured-icon-box style1 ttm-bgcolor-darkgrey">
                                <div class="featured-icon">
                                    <div class="ttm-icon ttm-icon_element-color-white ttm-icon_element-size-md">
                                        <i class="themifyicon ti-user"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    {{--                                            <div class="featured-title">--}}
                                    {{--                                                <h5>Поддержка 24/7</h5>--}}
                                    {{--                                            </div>--}}
                                    <div class="featured-desc">
                                        <p>Постоянным клиентам скидки и спецпредожения</p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <!-- featured-icon-box -->
                            <div class="featured-icon-box style1 ttm-bgcolor-grey">
                                <div class="featured-icon">
                                    <div class="ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                        <i class="themifyicon ti-settings"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    {{--                                            <div class="featured-title">--}}
                                    {{--                                                <h5>Free Shipping</h5>--}}
                                    {{--                                            </div>--}}
                                    <div class="featured-desc">
                                        <p>Прямые поставки от производителя</p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- row end -->
    </div>
</section>
