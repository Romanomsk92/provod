<section class="product-section clearfix">
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-lg-7 col-md-8 ml-auto mr-auto">
                <!-- section title -->
                <div class="section-title title-style-center_text style2">
                    <div class="title-header">
                        <h1 class="title">Поставка кабельно-проводниковой продукции</h1>
                    </div>
                </div><!-- section title end -->
            </div>
        </div><!-- row end -->
        <div class="row">
            <div class="col-lg-12">
                <div class="ttm-tabs tabs-for-products" data-effect="fadeIn">
                    <ul class="tabs clearfix">
                        @if(isset($hit) and $hit)<li class="tab active"><a href="#">Хит продаж</a></li>@endif
                        @if(isset($stock) and $stock)<li class="tab"><a href="#">Акции</a></li>@endif
                    </ul>
                    <div class="content-tab">
                        <!-- content-inner -->
                        <div class="content-inner">
                            <div class="products row">

                                @if(isset($hit) and $hit)
                                    @foreach($hit as $item)
                                        @include('frontend.parts.product')
                                    @endforeach
                                @endif

                            </div>
                        </div>
                        <!-- content-inner -->
                        <div class="content-inner">
                            <div class="products row">
                            @if(isset($stock) and $stock)
                                @foreach($stock as $item)
                                        @include('frontend.parts.product')
                                @endforeach
                            @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- product-section end-->
