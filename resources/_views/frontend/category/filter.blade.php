<aside class="widget widget-price-filter">
    <h3 class="widget-title"><i class="fa fa-bars"></i>Фильтр</h3>
    <div class="product_content">
        <div class="price_slider_wrapper">
            <h5>Цена :</h5>
            <form id="category_filter" method="get">
                <div id="slider-range" class="price-filter-range"></div>
                <!-- price_slider_amount -->
                <div class="price_slider_amount">
                    <input type="text" id="min_price" name="min_price" value="{{ $products->min('price') }}" data-min="{{ $products->min('price') }}" placeholder="От" />
                    <input type="text" id="max_price" name="max_price" value="{{ $products->max('price') }}" data-max="{{ $products->max('price') }}" placeholder="До" />
                    <button type="submit" class="button">Принять</button>
                </div>
                <input name="category" type="hidden" value="{{ $category->id }}">
            </form>
        </div>

        @foreach($attributes as $attribute)
            <div class="{{ $attribute['alias'].'_wrapper' }} filter-block">
                <h5>{{ $attribute['name'] }}</h5>
                <div class="choose-option-point">
                    <select name="{{ $attribute['alias'] }}">
                        <option value="">Выберите</option>

                        @foreach($attribute['data'] as $attr)
                            <option value="{{ $attr }}">{{ $attr }}</option>
                        @endforeach

                    </select>
                </div>
            </div>
        @endforeach

{{--        <div class="color_wrapper">--}}
{{--            <h5>color</h5>--}}
{{--            <div class="choose-option-point">--}}
{{--                <select>--}}
{{--                    <option value="">Choose color</option>--}}
{{--                    <option value="">Aqua</option>--}}
{{--                    <option value="">Black</option>--}}
{{--                    <option value="">White</option>--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</aside>
