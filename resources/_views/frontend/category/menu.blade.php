<aside class="widget menu-content">
    <h3 class="widget-title"><i class="fa fa-bars"></i>Каталог</h3>
    <ul class="menu-vertical">
        @include('frontend.parts.categories')
    </ul>
</aside>
