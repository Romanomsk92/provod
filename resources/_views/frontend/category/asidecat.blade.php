<aside class="widget products top-rated-products">
    <h3 class="widget-title">Featured Products</h3>
    <ul class="product-list-widget product">
        <li>
            <a href="#"><img src="https://via.placeholder.com/271x300?text=271x300+Pro+view1" alt=""></a>
            <div class="product-content-box">
                <div class="star-ratings">
                    <ul class="rating">
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                    </ul>
                </div>
                <a class="product-title" href="product-layout1.html">
                    <h2>Rotary Hammer</h2>
                </a>
                <span class="price">
                                            <span class="product-Price-amount">
                                                <span class="product-Price-currencySymbol">$</span>80.00
                                            </span>
                                        </span>
            </div>
        </li>
        <li>
            <a href="#"><img src="https://via.placeholder.com/271x300?text=271x300+Pro+view1" alt=""></a>
            <div class="product-content-box">
                <div class="star-ratings">
                    <ul class="rating">
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                        <li><i class="fa fa-star-o"></i></li>
                    </ul>
                </div>
                <a class="product-title" href="product-layout1.html">
                    <h2>Tape Measure</h2>
                </a>
                <span class="price">
                                            <span class="product-Price-amount">
                                                <span class="product-Price-currencySymbol">$</span>24.00
                                            </span>
                                        </span>
            </div>
        </li>
    </ul>
</aside>
