<div class="products row">
    @foreach($products as $item)
        @include('frontend.parts.product')
    @endforeach
</div>
