@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')


    <!--site-main start-->
    <div class="site-main">

        <!-- checkout-section -->
        <section class="checkout-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">

                    <div class="col-lg-12 cart-page">
                        <form name="checkout" method="post" class="checkout row" id="placeoreder" action="{{ route('placeoreder') }}">
                            <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <div class="form-row">
                                        <label>Ф.И.О.</label>
                                        <input type="text" class="input-tex" name="name" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row">
                                        <label>Контактный телефон</label>
                                        <input type="tel" class="input-tex" name="phone" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row">
                                        <label>Email</label>
                                        <input type="email" class="input-tex" name="Email" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row" id="order_comments_field">
                                        <label>Адрес доставки:</label>
                                        <textarea name="delivery" class="input-text " id="delivery" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row" id="order_comments_field">
                                        <label>Комментарии</label>
                                        <textarea name="comment" class="input-text " id="comment" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="checkbox">
                                        <label data-target="#collapseOne">
                                            <input name="pickup" value="1" type="checkbox"/> <span class="ttm-textcolor-dark">Отметьте если хотите самостоятельно забрать товар с нашего склада.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="pt-30 res-991-pt-15">
                                    <div class="content-sec-head-style">
                                        <div class="content-area-sec-title">
                                            <h5>Ваш заказ</h5>
                                        </div>
                                    </div>
                                    <div id="order_review" class="checkout-review-order">
                                        <table class="cart_table checkout-review-order-table">
                                            <thead>
                                            <tr>
                                                <th class="product-name">Товар</th>
                                                <th class="product-total">Итого</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cart as $item)
                                                <tr class="cart_item">
                                                    <td class="product-name">{{ $item['name'] }}&nbsp;
                                                        <strong class="product-quantity">× {{ $item['quantity'] }}</strong>
                                                    </td>
                                                    <td class="product-total">
                                                        <span class="Price-amount">
                                                            {{ $item['price'] * $item['quantity'] }}<span class="Price-currencySymbol">₽</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>

                                            <tr class="order-total">
                                                <th>Итого</th>
                                                <td>
                                                    <strong>
                                                        <span class="woocommerce-Price-amount amount">
                                                            {{ $cart_summ }}<span class="woocommerce-Price-currencySymbol">₽</span>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9"></div>
                            <div class="col-lg-3">
                                <div class="text-right place-order mt-30">
                                    <button type="submit" class="button" name="checkout_place_order" id="place_order" value="Заказать" data-value="Заказать">Заказать</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- checkout-section end -->

    </div><!--site-main end-->
@endsection
