@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')


    <!--site-main start-->
    <div class="site-main">

        <!-- sidebar -->
        <section class="sidebar ttm-sidebar-left clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-3 widget-area sidebar-left">
                        @include('frontend.category.menu')
                        @include('frontend.category.filter')
{{--                        @include('frontend.category.asidecat')--}}

                    </div>
                    <div class="col-lg-9 content-area">

                        <div data-url="{{ $category->Url }}" class="products products-fitter">
                            <div class="ttm-tabs">

                                <div class="content-tab">
                                    <div class="content-inner">
                                        @include('frontend.category.products')
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-lg-12">--}}
{{--                        <div class="pagination-block">--}}
{{--                            <span class="page-numbers current">1</span>--}}
{{--                            <a class="page-numbers" href="#">2</a>--}}
{{--                            <a class="page-numbers" href="#">3</a>--}}
{{--                            <a class="next page-numbers" href="#"><i class="ti ti-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div><!-- row end -->
            </div>
        </section>
        <!-- sidebar end -->

    </div><!--site-main end-->
@endsection
