@extends('frontend.layout')

@section('page')

    @include('frontend.mainpage.slider')
    <!-- END REVOLUTION SLIDER -->

    <!--site-main start-->
    <div class="site-main">

        <!-- row-top-section -->
        @include('frontend.mainpage.sectionone')
        <!-- row-top-section end -->

        <!-- product-section -->
        @include('frontend.mainpage.productsection')

        <!-- testimonial-section -->
        @include('frontend.mainpage.testimonial')
        <!-- testimonial-section end-->

        <!-- featured-product-section -->
        @include('frontend.mainpage.categories')

        <!-- featured-product-section end -->

    </div><!--site-main end-->


@endsection
