@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')


    <!--site-main start-->
    <div class="site-main">

        <!--google_map-->
        <div id="google_map" class="google_map">
            <div class="map_container">
                <div id="map"></div>
            </div>
        </div>

        <!-- google_map end -->
        <section class="contact-section bg-layer bg-layer-equal-height clearfix">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-lg-8 col-md-7">
                        <div class="ttm-col-bgcolor-yes ttm-bg ttm-bgcolor-grey spacing-2">
                            <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>
                            <div class="layer-content">
                                <!-- section title -->
                                <div class="section-title style2">
                                    <div class="title-header">
                                        <h2 class="title">Контакты</h2>
                                    </div>
                                </div><!-- section title end -->
                                <form id="ttm-contactform" class="ttm-contactform wrap-form clearfix ajax-form" method="post" action="{{ route('formsend') }}">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>
                                                <span class="text-input"><i class="ttm-textcolor-darkgrey ti-user"></i><input name="your-name" type="text" value="" placeholder="Имя" required="required"></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>
                                                <span class="text-input"><i class="ttm-textcolor-darkgrey ti-mobile"></i><input name="your-phone" type="text" value="" placeholder="Телефон" required="required"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>
                                                <span class="text-input"><i class="ttm-textcolor-darkgrey ti-email"></i><input name="email" type="email" value="" placeholder="Email" required="required"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <label>
                                        <span class="text-input"><i class="ttm-textcolor-darkgrey ti-comment"></i><textarea name="message" rows="3" cols="40" placeholder="Сообщение" required="required"></textarea></span>
                                    </label>
                                    <input name="submit" type="submit" id="submit" class="submit ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor" value="Отправить">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <div class="ttm-col-bgcolor-yes ttm-bg ttm-bgcolor-skincolor spacing-3">
                            <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>
                            <div class="layer-content">
                                <div class="box-header">
                                    <div class="box-icon">
                                        <i class="fa fa-paper-plane"></i>
                                    </div>
                                </div>
                                <h4>Контактная информация</h4>
                                <ul class="ttm_contact_widget_wrapper">
                                    <li><i class="ttm-textcolor-highlight ti ti-location-pin"></i>г. Казань ул. Восстания, д. 104</li>
                                    <li><i class="ttm-textcolor-highlight ti ti-headphone"></i><a href="tel:+78435574849">+7 (843) 557-48-49</a></li>
                                    <li><i class="ttm-textcolor-highlight ti ti-files"></i><a href="tel:+78432259091">+7 (843) 225-90-91</a></li>
                                    <li><i class="ttm-textcolor-highlight ti ti-email"></i><a href="mailto:info@pro-vod.ru">info@pro-vod.ru</a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div><!-- row end -->
            </div>
        </section>


    </div><!--site-main end-->

@endsection
