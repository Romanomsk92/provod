<div class="pt-35 related products">
    <div class="row">
        <div class="col-lg-12">
            <div class="content-sec-head-style">
                <div class="content-area-sec-title">
                    <h5>Самые продаваемые товары</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <!-- slick_slider -->
            <div class="slick_slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 4, "arrows":true, "autoplay":true, "infinite":false}'>
                @foreach($topsellers as $item)
                    <div class="product">
                        <div class="product-box">
                            <!-- product-box-inner -->
                            <div class="product-box-inner">
                                <div class="product-image-box">
                                    <img class="img-fluid pro-image-front" src="{{ $item->photo }}" alt="">
                                    <img class="img-fluid pro-image-back" src="{{ $item->photo }}" alt="">
                                </div>
                                <div class="product-btn-links-wrapper">
                                    <div class="product-btn"><a href="#" class="add-to-cart-btn tooltip-top" data-tooltip="Добавить в корзину"><i class="ti ti-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div><!-- product-box-inner end -->
                            <div class="product-content-box">
                                <a class="product-title" href="{{ $item->Url }}">
                                    <h2>{{ $item->name }}</h2>
                                </a>
                                <div class="star-ratings">
                                    <ul class="rating">
                                        @for($i = 0; $i < $item->rating; $i++)
                                            <li class="fa fa-star"></li>
                                        @endfor
                                        @for($i = 0; $i < 5 - $item->rating; $i++)
                                            <li class="fa fa-star-o"></li>
                                        @endfor
                                    </ul>
                                </div>
                                <span class="price">
                                    <span class="product-Price-amount">
                                        {{ $item->price }}<span class="product-Price-currencySymbol">₽</span>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div><!-- slick_slider end -->
        </div>
    </div>
</div>
