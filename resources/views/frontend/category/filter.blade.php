@if(count($products))
    <aside class="widget widget-price-filter">
        <h3 class="widget-title"><i class="fa fa-bars"></i>Фильтр</h3>
        <div class="product_content">
            <form id="category_filter" method="get">
                <div class="price_slider_wrapper">
                    <h5>Цена :</h5>
                    <div id="slider-range" class="price-filter-range"></div>
                    <!-- price_slider_amount -->
                    <div class="price_slider_amount">
                        <input type="text" id="min_price" name="min_price" value="{{ $products->min('price') }}" data-min="{{ $products->min('price') }}" placeholder="От" />
                        <input type="text" id="max_price" name="max_price" value="{{ $products->max('price') }}" data-max="{{ $products->max('price') }}" placeholder="До" />
                        <button type="submit" class="button">Принять</button>
                    </div>
                    <input name="category" type="hidden" value="{{ $category->id }}">
                </div>

                @foreach($attributes as $attribute)
                    @if($attribute['alias'] == 'minimal-naya-rabochaya-temperatura' || $attribute['alias'] == 'maksimal-naya-rabochaya-temperatura')
                        @php
                            $minval = intval(preg_replace('/(\+)/', '', current($attribute['data'])));
                            $maxval = intval(preg_replace('/(\+)/', '', end($attribute['data'])));
                        @endphp

                        @if($maxval < $minval)
                            @php list($maxval, $minval) = [$minval, $maxval]; @endphp
                        @endif

                        @if($minval != $maxval)

                        <div class="{{ $attribute['alias'].'_wrapper' }} filter-block price_slider_wrapper">
                            <h5>{{ $attribute['name'] }}</h5>
                            <div class="">
                                <div class="slider-range slide-{{ $attribute['alias'] }} price-filter-range"></div>
                                <div class="price_slider_amount">
                                    <input type="text" class="min_value" name="{{ 'min_'.$attribute['alias'] }}" value="{{ $minval }}" data-min="{{ $minval }}" placeholder="От" />
                                    <input type="text" class="max_value" name="{{ 'max_'.$attribute['alias'] }}" value="{{ $maxval }}" data-max="{{ $maxval }}" placeholder="До" />
                                    <button type="submit" class="button">Принять</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    @else
                        <div class="{{ $attribute['alias'].'_wrapper' }} filter-block">
                            <h5>{{ $attribute['name'] }}</h5>
                            <div class="choose-option-point">
                                <select name="{{ $attribute['alias'] }}">
                                    <option value="">Выберите</option>

                                    @foreach($attribute['data'] as $attr)
                                        <option @if(isset($post) && isset($post[$attribute['alias']]) && $post[$attribute['alias']] == $attr) selected @endif value="{{ $attr }}">{{ $attr }}</option>
                                    @endforeach

                                </select>
                                <button type="submit" class="button accept-btn">Принять</button>
                            </div>
                        </div>
                    @endif

                @endforeach

                <a href="{{ $category->url }}" class="button clear-btn">Сбросить</a>

            </form>

            {{--        <div class="color_wrapper">--}}
            {{--            <h5>color</h5>--}}
            {{--            <div class="choose-option-point">--}}
            {{--                <select>--}}
            {{--                    <option value="">Choose color</option>--}}
            {{--                    <option value="">Aqua</option>--}}
            {{--                    <option value="">Black</option>--}}
            {{--                    <option value="">White</option>--}}
            {{--                </select>--}}
            {{--            </div>--}}
            {{--        </div>--}}
        </div>
    </aside>

@endif
