<div class="products row products-shop">
    @foreach($products as $item)
        @include('frontend.parts.product', ['class'=>'col-md-4 col-sm-6 col-12'])
    @endforeach
</div>
