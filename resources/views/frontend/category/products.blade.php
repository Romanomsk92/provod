@if(isset($scategories) && $scategories)
    <div class="products row">
        @foreach($scategories as $category)
            <div class="product col-md-3 col-sm-6 col-xs-12">
                <div class="product-box">
                    <!-- product-box-inner -->
                    <div class="product-box-inner">
                        <div class="product-image-box">
                            <img class="img-fluid pro-image-front" src="{{ asset($category->photo) }}" alt="{{ $category->name }}" title="{{ $category->name }}">
                            <img class="img-fluid pro-image-back" src="{{ asset($category->photo) }}" alt="{{ $category->name }}" title="{{ $category->name }}">
                        </div>
                    </div><!-- product-box-inner end -->
                    <div class="product-content-box">
                        <a class="product-title" href="{{ $category->Url }}">
                            <h2>{{ $category->name }}</h2>
                        </a>
                    </div>
                </div>
            </div>
            <!-- product end -->
        @endforeach
    </div>
@endif

<div class="products row products-shop">
    @foreach($products as $item)
        @include('frontend.parts.product', ['class'=>'col-md-4 col-sm-6 col-12'])
    @endforeach
</div>
