<div id="citySelector" class="p-5" style="display: none; ">
    <div class=" d-flex flex-wrap">
        <div class="city-selector_close" data-fancybox-close>
            <svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"></path></svg>
        </div>
        <div class="col-12 city-selector_header mb-3">
            <h4>Выбор города</h4>
        </div>
        @foreach($cities as $k => $fw)
            <div class="city-group col-12 col-md-6 col-lg-4">
                <div class="city-group_header">
                    {{ $k }}
                </div>
                <div class="city-group_list">
                    <ul>
                        @foreach($fw as $c)
                            @if($c->default == 1)
                                <li><a href="{{'https://'.$_SERVER['APP_URL']}}">{{ $c->title }}</a></li>
                            @else
                                <li><a href="{{'https://'.$c->domain.'.'.$_SERVER['APP_URL']}}">{{ $c->title }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
</div>
