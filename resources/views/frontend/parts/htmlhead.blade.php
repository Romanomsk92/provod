<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>@if(isset($meta_title)) {{ $meta_title }} @endif</title>
<meta name="keywords" content="@if(isset($meta_keywords)) {{ $meta_keywords }} @endif" />
<meta name="description" content="@if(isset($meta_description)) {{ $meta_description }} @endif" />

@if(isset($meta)) {!! $meta !!} @endif

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- favicon icon -->
<link rel="shortcut icon" href="images/favicon.png" />
<meta name="yandex-verification" content="efe592823f244f08" />
