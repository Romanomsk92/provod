<!--header start-->
<header id="masthead" class="header ttm-header-style-01">
    <!-- top_bar -->
    @include('frontend.parts.topbar')
    <!-- header_main -->
    @include('frontend.parts.headermain')
    <!-- site-header-menu -->
    <div id="site-header-menu" class="site-header-menu ttm-bgcolor-darkgrey clearfix">
        <div class="site-header-menu-inner stickable-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-6">
                        <div class="main_nav_content d-flex flex-row">
                            <div class="cat_menu_container">
                                <a href="#" class="cat_menu d-flex flex-row align-items-center">
                                    <div class="cat_icon"><i class="fa fa-bars"></i></div>
                                    <div class="cat_text">
                                        <h4>Каталог товаров</h4>
                                    </div>
                                    <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.6098 5.83709L0.159755 1.29221C0.0567475 1.1871 -4.57559e-08 1.04677 -3.92157e-08 0.897152C-3.26756e-08 0.74753 0.0567475 0.607207 0.159755 0.50209L0.487394 0.167391C0.700889 -0.0503998 1.04788 -0.0503998 1.26105 0.167391L4.99793 3.98384L8.73895 0.163157C8.84196 0.0580392 8.97928 3.92497e-07 9.1257 3.98897e-07C9.27228 4.05304e-07 9.4096 0.0580392 9.51269 0.163157L9.84025 0.497855C9.94325 0.603056 10 0.743296 10 0.892918C10 1.04254 9.94325 1.18286 9.84025 1.28798L5.38614 5.83709C5.2828 5.94246 5.14484 6.00033 4.99817 6C4.85094 6.00033 4.71305 5.94246 4.6098 5.83709Z" fill="#02112B"/>
                                    </svg>
                                </a>

                                <ul class="cat_menu_list menu-vertical">
                                    <li><a href="#" class="close-side"><i class="fa fa-times"></i></a></li>

                                    @include('frontend.parts.categories')
                                </ul>
                            </div>

                            <!--site-navigation -->
                            <div id="site-navigation" class="site-navigation">
                                <div class="btn-show-menu-mobile menubar menubar--squeeze">
                                    <span class="menubar-box">
                                        <span class="menubar-inner"></span>
                                    </span>
                                </div>
                                <!-- menu -->
                                <nav class="menu menu-mobile" id="menu">
                                    <ul class="nav">
                                        @foreach($pages as $page)
                                            <li><a href="@if($page->alias == '/') / @else /{{ $page->alias }} @endif">{{ $page->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </nav>
                            </div>
                            <!-- site-navigation end-->
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 d-block">
                        <div class="header_search"><!-- header_search -->
                            <div class="header_search_content">
                                <div id="search_block_top" class="search_block_top">
                                    <form id="searchbox" method="get" action="/search">
                                        <input class="search_query form-control" type="text" id="search_query_top" name="search" placeholder="Что вы ищите?" value="">

                                        <button type="submit" class="btn btn-default button-search"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- header_search end -->
                    </div>
                </div>
            </div>
        </div>
    </div><!-- site-header-menu end -->
</header>
<!--header end-->
