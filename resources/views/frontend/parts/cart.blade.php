<div
    @if(isset($class))
    class="{{ $class }}"
    @else
    class="cart dropdown"
    @endif
>
    <div class="dropdown_link cart-block d-flex flex-row align-items-center justify-content-end" data-toggle="dropdown">
        <div class="cart_icon">
            <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.1822 8.20281C10.707 8.20281 11.1689 7.91581 11.4068 7.48181L13.9122 2.93881C13.9711 2.83271 14.0014 2.71304 13.9999 2.59164C13.9985 2.47025 13.9653 2.35135 13.9038 2.24669C13.8423 2.14203 13.7547 2.05525 13.6493 1.99494C13.544 1.93462 13.4247 1.90287 13.3033 1.90281H2.94619L2.28837 0.502808H0V1.90281H1.39962L3.91892 7.21581L2.97418 8.92381C2.46332 9.86181 3.13513 11.0028 4.19884 11.0028H12.5965V9.60281H4.19884L4.96864 8.20281H10.1822ZM3.611 3.30281H12.1137L10.1822 6.80281H5.26955L3.611 3.30281ZM4.19884 11.7028C3.42905 11.7028 2.80623 12.3328 2.80623 13.1028C2.80623 13.8728 3.42905 14.5028 4.19884 14.5028C4.96864 14.5028 5.59846 13.8728 5.59846 13.1028C5.59846 12.3328 4.96864 11.7028 4.19884 11.7028ZM11.1969 11.7028C10.4271 11.7028 9.80429 12.3328 9.80429 13.1028C9.80429 13.8728 10.4271 14.5028 11.1969 14.5028C11.9667 14.5028 12.5965 13.8728 12.5965 13.1028C12.5965 12.3328 11.9667 11.7028 11.1969 11.7028Z" fill="#02112B"/>
            </svg>
            <div class="cart_count">{{ $cart_quantity }}</div>
        </div>
        <div class="cart_content">
            <div class="cart_price"><a href="#">{{ number_format($cart_summ, '0', '.', ' ') }} ₽</a></div>
        </div>
    </div>
    @if(isset($cart))
        <aside class="widget_shopping_cart dropdown_content">
            <ul class="cart-list">
                @foreach($cart as $item)
                    <li>
                        <a href="#" class="photo"><img src="{{ asset($item['photo']) }}" class="cart-thumb" alt="" /></a>
                        <h6><a href="{{ $item['url'] }}">{{ $item['name'] }}</a></h6>
                        <p>{{ $item['quantity'] }} шт - <span class="price">{{ $item['quantity'] * $item['price'] }} ₽</span></p>
                    </li>
                @endforeach


                <li class="total">
                    <span class="pull-right"><strong>Итого</strong>: {{ $cart_summ }} ₽</span>
                    <a href="/cart" class="btn btn-default btn-cart">Корзина</a>
                </li>
            </ul>
        </aside>
    @endif
</div>
