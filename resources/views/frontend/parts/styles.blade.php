<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" rel="stylesheet" type="text/css">


<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}"/>

<!-- animate -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}"/>

<!-- fontawesome -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}"/>

<!-- themify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/themify-icons.css') }}"/>

<!-- slick -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">

<!-- slick -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}">

<!-- REVOLUTION LAYERS STYLES -->

<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/layers.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}">

<!-- magnific-popup -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/magnific-popup.css') }}"/>

<!-- megamenu -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/megamenu.css') }}">

<!-- shortcodes -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/shortcodes.css') }}"/>

<!-- main -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}"/>

<!-- responsive -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}"/>

<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.fancybox.min.css') }}"/>

<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}"/>
