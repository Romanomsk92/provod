@foreach(\App\Category::where('parent_id', 0)->orWhere('parent_id', NULL)->orderBy('order')->get() as $category)
    @php
        $childs = \App\Category::where('parent_id', $category->id)->get();
    @endphp
    @if(count($childs))

        <li class="parent">
            <a href="/{{ $category->alias }}">{{ $category->name }}</a>

            <div class="sub-menu megamenu column3">
                <ul class="list-unstyled childs_1">

                    @foreach($childs as $child)


                        <li class="title">

                            <a href="/{{ $category->alias }}/{{ $child->alias }}">
                                {{ $child->name }}
                            </a>

                            @if($childer = \App\Category::where('parent_id', $child->id)->get())
                                <div class="sub-menu">

                                    <ul class="list-unstyled childs_2">
                                        @foreach($childer as $item)
                                            <li>
                                                <a href="/{{ $category->alias }}/{{ $child->alias }}/{{ $item->alias }}">
                                                    {{ $item->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>

                                </div>

                            @endif

                        </li>

                    @endforeach

                </ul>

            </div>

        </li>

    @else
        <li><a href="/{{ $category->alias }}">{{ $category->name }}</a></li>
    @endif
@endforeach
