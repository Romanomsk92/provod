<div class="product pcard @if(isset($class)) {{ $class }} @else col-md-3 col-sm-6 col-12 @endif">
    <div class="product-box">
        <!-- product-box-inner -->
        <div class="product-box-inner">
            <div class="product-image-box">
                @if($item->stock == 1)
                    <div class="onsale">Распродажа!</div>
                @endif
                <img class="img-fluid pro-image-front" src="{{ asset($item->photo) }}" alt="{{ $item->name }}" title="{{ $item->name }}">
                <img class="img-fluid pro-image-back" src="{{ asset($item->photo) }}" alt="{{ $item->name }}" title="{{ $item->name }}">
            </div>
            {{--            <div class="product-btn-links-wrapper">--}}
            {{--                <div class="product-btn">--}}
            {{--                    <a href="#"--}}
            {{--                       class="add-to-cart-btn tooltip-top"--}}
            {{--                       data-id="{{ $item->id }}"--}}
            {{--                       data-tooltip="Добавить в корзину">--}}
            {{--                            <i class="ti ti-shopping-cart"></i>--}}
            {{--                    </a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
        <div class="product-content-box">
            <span class="warehouse @if($item->warehouse > 0) instock @endif">@if($item->warehouse > 0) В наличии @else Под заказ @endif</span>
            <a class="product-title" href="{{ $item->Url }}">
                <h2>{{ $item->name }}</h2>
            </a>
            {{--            <div class="star-ratings">--}}
            {{--                <ul class="rating">--}}
            {{--                    @for($i = 0; $i < $item->rating; $i++)--}}
            {{--                        <li class="fa fa-star"></li>--}}
            {{--                    @endfor--}}
            {{--                    @for($i = 0; $i < 5 - $item->rating; $i++)--}}
            {{--                        <li class="fa fa-star-o"></li>--}}
            {{--                    @endfor--}}
            {{--                </ul>--}}
            {{--            </div>--}}
            @if(isset($item->attributes) && $item->attributes)
                <div class="product-att_block">
                    @foreach($item->attributes as $key => $attribute)
                        @if($attribute->id == 4 || $attribute->id == 6 || $attribute->id == 7 || $attribute->id == 8 )
                            <div class="product-att_item">
                                <div class="name">{{ $attribute->name }}</div>
                                <div class="value">{{ $attribute->pivot->value }}</div>
                            </div>
                        @endif
                    @endforeach

                    <a style="font-size: 12px; color: #02112b; text-decoration: underline;" href="{{ $item->Url }}">
                        Больше свойств..
                    </a>
                </div>
            @endif

            <div class="price d-flex justify-content-between">
                <div class="product-Price-amount">
                    {{ $item->price }}<span class="product-Price-currencySymbol">₽</span><span> за метр</span>
                </div>
                <div class="add-to-cart @if(isset($cart) && array_key_exists($item->id, $cart)) incart @endif">
                    <a href="#"
                       class="add-to-cart-btn tooltip-top"
                       data-id="{{ $item->id }}"
                       data-tooltip="@if(isset($cart) && array_key_exists($item->id, $cart)) Товар в корзине @else Добавить в корзину @endif">
                        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.1822 7.7C10.707 7.7 11.1689 7.413 11.4068 6.979L13.9122 2.436C13.9711 2.3299 14.0014 2.21023 13.9999 2.08884C13.9985 1.96745 13.9653 1.84854 13.9038 1.74388C13.8423 1.63923 13.7547 1.55244 13.6493 1.49213C13.544 1.43181 13.4247 1.40006 13.3033 1.4H2.94619L2.28837 0H0V1.4H1.39962L3.91892 6.713L2.97418 8.421C2.46332 9.359 3.13513 10.5 4.19884 10.5H12.5965V9.1H4.19884L4.96864 7.7H10.1822ZM3.611 2.8H12.1137L10.1822 6.3H5.26955L3.611 2.8ZM4.19884 11.2C3.42905 11.2 2.80623 11.83 2.80623 12.6C2.80623 13.37 3.42905 14 4.19884 14C4.96864 14 5.59846 13.37 5.59846 12.6C5.59846 11.83 4.96864 11.2 4.19884 11.2ZM11.1969 11.2C10.4271 11.2 9.80429 11.83 9.80429 12.6C9.80429 13.37 10.4271 14 11.1969 14C11.9667 14 12.5965 13.37 12.5965 12.6C12.5965 11.83 11.9667 11.2 11.1969 11.2Z" fill="#FDD200"/>
                        </svg>
                        @if(isset($cart) && array_key_exists($item->id, $cart))
                            <span>В корзине</span>
                        @else
                            <span>В корзину</span>
                        @endif
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
