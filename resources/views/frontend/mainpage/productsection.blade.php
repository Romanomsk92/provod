<section class="product-section clearfix">
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-lg-7 col-md-8 ml-auto mr-auto">
                <!-- section title -->
                <div class="section-title title-style-center_text style2">
                    <div class="title-header">
                        <h1 class="title">Хиты продаж</h1>
                    </div>
                </div><!-- section title end -->
            </div>
        </div><!-- row end -->
        <div class="row">
                @if(isset($hit) and $hit)
                    @foreach($hit as $item)
                        @include('frontend.parts.product')
                    @endforeach
                @endif
        </div>
    </div>
</section><!-- product-section end-->
<section class="product-section clearfix">
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-lg-7 col-md-8 ml-auto mr-auto">
                <!-- section title -->
                <div class="section-title title-style-center_text style2">
                    <div class="title-header">
                        <h1 class="title">Акции</h1>
                    </div>
                </div><!-- section title end -->
            </div>
        </div><!-- row end -->
        <div class="row">
            @if(isset($stock) and $stock)
                @foreach($stock as $item)
                    @include('frontend.parts.product')
                @endforeach
            @endif
        </div>
    </div>
</section><!-- product-section end-->
