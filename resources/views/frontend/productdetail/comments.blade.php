<div id="comments">
    <ol class="commentlist">
        @foreach($reviews as $item)
            <li class="review">
                <div class="comment_container">
                    <div class="avatar">{!! substr($item->name, 0, 1) !!}</div>
                    <div class="comment-text">
                        <ul class="star-rating">
                            @for($i = 0; $i < $item->rating; $i++)
                            <li class="fa fa-star"></li>
                            @endfor
                            @for($i = 0; $i < 5 - $item->rating; $i++)
                            <li class="fa fa-star-o"></li>
                            @endfor
                        </ul>
                        <p class="meta">
                            <strong class="eview__author">{{ $item->name }} </strong><span class="review__dash">–</span>
                            <time class="woocommerce-review__published-date" datetime="{{ $item->created_at }}">{!! date('d.m.Y', strtotime($item->created_at)) !!}</time>
                        </p>
                        <div class="description">
                            <p>{{ $item->comment }}</p>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ol>
</div>
