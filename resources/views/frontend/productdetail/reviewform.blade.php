<div id="review_form_wrapper">
    <div class="comment-respond">
        <span class="comment-reply-title">Добавить отзыв
        </span>
        <form action="/addComment" method="post" id="commentform" class="comment-form">
            <input id="pid" name="pid" type="hidden" value="{{ $product->id }}" required="">

            <div class="comment-form-rating">
                {{--                                                            <label for="rating">Оценка</label>--}}
                <ul class="stars">
                    {{--                                                                <li class="fa fa-star-o"></li>--}}
                    {{--                                                                <li class="fa fa-star-o"></li>--}}
                    {{--                                                                <li class="fa fa-star-o"></li>--}}
                    {{--                                                                <li class="fa fa-star-o"></li>--}}
                    {{--                                                                <li class="fa fa-star-o"></li>--}}
                </ul>
                <select name="rating" id="rating" required="" tabindex="-1">
                    <option value="">Оценка…</option>
                    <option value="5">Отлично</option>
                    <option value="4">Хорошо</option>
                    <option value="3">Средний</option>
                    <option value="2">Не так уж плохо</option>
                    <option value="1">Плохо</option>
                </select>
            </div>
            <p class="comment-form-comment">
                <label for="comment">Отзыв<span class="required">*</span></label>
                <textarea id="comment" name="comment" cols="45" rows="8" required=""></textarea>
            </p>
            <p class="comment-form-author">
                <label for="author">Имя&nbsp;<span class="required">*</span></label>
                <input id="author" name="author" type="text" value="" required="">
            </p>
            <p class="comment-form-email">
                <label for="email">Email&nbsp;<span class="required">*</span></label>
                <input id="email" name="email" type="email" value="" required="">
            </p>
            <p class="form-submit">
                <input name="submit" type="submit" class="submit" value="Отправить">
            </p>
        </form>
    </div>
</div>
