@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')


    <!--site-main start-->
    <div class="site-main position-relative">

        <!--google_map-->
        <div class="d-flex position-absolute w-100 h-100">
            <div id="google_map" class="google_map offset-md-6 col-md-6 pr-0">
                <div class="map_container h-100">
                    <div id="map" class=" h-100"></div>
                </div>
            </div>
        </div>

        <!-- google_map end -->
        <section class="bg-layer bg-layer-equal-height clearfix">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <div class="ttm-col-bgcolor-yes ttm-bg">
                            <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>
                            <div class="layer-content">
                                <div class="box-header">
                                    <div class="box-icon">
                                        <i class="fa fa-paper-plane"></i>
                                    </div>
                                </div>
                                <ul class="ttm_contact_widget_wrapper row">
                                    <li class="col-12 col-md-6">
                                        <div class="d-flex align-items-center mb-2">
                                            <i class="ttm-textcolor-highlight ti ti-location-pin"></i>
                                            <span class="title">Адрес:</span>
                                        </div>
                                        <div class="cont">г. Казань ул. Восстания, д. 104</div>
                                    </li>
                                    <li class="col-12 col-md-6">
                                        <div class="d-flex align-items-center mb-2">
                                            <i class="ttm-textcolor-highlight ti ti-headphone"></i>
                                            <span class="title">Телефон:</span>
                                        </div>
                                        <div class="cont">
                                            <a href="tel:+78435574849">+7 (843) 557-48-49</a>
                                        </div>
                                    </li>
                                    <li class="col-12 col-md-6">
                                        <div class="d-flex align-items-center mb-2">
                                            <i class="ttm-textcolor-highlight ti ti-files"></i>
                                            <span class="title">Телефон (факс):</span>
                                        </div>
                                        <div class="cont">
                                            <a href="tel:+78432259091">+7 (843) 225-90-91</a>
                                        </div>
                                    </li>
                                    <li class="col-12 col-md-6">
                                        <div class="d-flex align-items-center mb-2">
                                            <i class="ttm-textcolor-highlight ti ti-email"></i>
                                            <span class="title">E-mail:</span>
                                        </div>
                                        <div class="cont">
                                            <a href="mailto:info@pro-vod.ru">info@pro-vod.ru</a>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-6" style="margin-top: 30px">
                        <div class="ttm-col-bgcolor-yes ttm-bg">
                            <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>
                            <div class="layer-content">
                                <!-- section title -->
                                <div class="">
                                    <div class="">
                                        <h4 class="title">Задайте интересующий Вас вопрос</h4>
                                    </div>
                                </div><!-- section title end -->
                                <form id="ttm-contactform" class="ttm-contactform wrap-form clearfix ajax-form" method="post" action="{{ route('formsend') }}">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>
                                                <span class="text-input">Имя *</span>
                                            </label>
                                            <input name="your-name" type="text" value="" required="required">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>
                                                <span class="text-input">Телефон *</span>
                                            </label>
                                            <input name="your-phone" type="text" value="" required="required">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>
                                                <span class="text-input">Email *</span>
                                            </label>
                                            <input name="email" type="email" value="" required="required">
                                        </div>
                                    </div>
                                    <label>
                                        <span class="text-input">Сообщение *</span>
                                    </label>
                                    <textarea name="message" rows="3" cols="40" required="required"></textarea>
                                    <input name="submit" type="submit" id="submit" class="submit ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor" value="Отправить">
                                </form>
                            </div>
                        </div>
                    </div>

                </div><!-- row end -->
            </div>
        </section>


    </div><!--site-main end-->

@endsection
