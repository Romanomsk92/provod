@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')

    <div class="site-main">

        <!-- single-product-section -->
        <section class="single-product-section layout-1 clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ttm-single-product-details">
                            <div class="ttm-single-product-info clearfix">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 ml-auto mr-auto">
                                        <div class="product-gallery easyzoom-product-gallery">
{{--                                            <div class="product-look-views left">--}}
{{--                                                <div class="mt-35 mb-35">--}}
{{--                                                    <ul class="thumbnails easyzoom-gallery-slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "arrows":true, "infinite":true, "vertical":true}'>--}}
{{--                                                        <li>--}}
{{--                                                            <a href="https://via.placeholder.com/768x1035?text=pro-view+01.jpg" data-standard="https://via.placeholder.com/768x1035?text=pro-view+01.jpg">--}}
{{--                                                                <img class="img-fluid" src="https://via.placeholder.com/768x1035?text=pro-view+01.jpg" alt="" />--}}
{{--                                                            </a>--}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <a href="https://via.placeholder.com/768x1035?text=pro-view+02.jpg" data-standard="https://via.placeholder.com/768x1035?text=pro-view+02.jpg">--}}
{{--                                                                <img class="img-fluid" src="https://via.placeholder.com/768x1035?text=pro-view+02.jpg" alt="" />--}}
{{--                                                            </a>--}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <a href="https://via.placeholder.com/768x1035?text=pro-view+03.jpg" data-standard="https://via.placeholder.com/768x1035?text=pro-view+03.jpg">--}}
{{--                                                                <img class="img-fluid" src="https://via.placeholder.com/768x1035?text=pro-view+03.jpg" alt="" />--}}
{{--                                                            </a>--}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <a href="https://via.placeholder.com/768x1035?text=pro-view+04.jpg" data-standard="https://via.placeholder.com/768x1035?text=pro-view+04.jpg">--}}
{{--                                                                <img class="img-fluid" src="https://via.placeholder.com/768x1035?text=pro-view+04.jpg" alt="" />--}}
{{--                                                            </a>--}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <a href="https://via.placeholder.com/768x1035?text=pro-view+05.jpg" data-standard="https://via.placeholder.com/768x1035?text=pro-view+05.jpg">--}}
{{--                                                                <img class="img-fluid" src="https://via.placeholder.com/768x1035?text=pro-view+05.jpg" alt="" />--}}
{{--                                                            </a>--}}
{{--                                                        </li>--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div class="product-look-preview-plus right">
                                                <div class="pl-35 res-767-pl-15">
                                                    <div class="">
                                                        <a data-fancybox="gallery" href="{{ asset($product->photo) }}">
                                                            <img class="img-fluid" src="{{ asset($product->photo) }}" alt="" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="summary entry-summary pl-30 res-991-pl-0 res-991-pt-40">
                                            <h2 class="product_title entry-title">
                                                {{ $product->name }}
                                                <span class="warehouse @if($product->warehouse > 0) instock @endif">@if($product->warehouse > 0) В наличии @else Под заказ @endif</span>
                                            </h2>
{{--                                            <div class="share-icons social-links">--}}
{{--                                                <span>Share :</span>--}}
{{--                                                <a href="#"><i class="ti ti-facebook"></i></a>--}}
{{--                                                <a href="#"><i class="ti ti-twitter-alt"></i></a>--}}
{{--                                                <a href="#"><i class="ti ti-google"></i></a>--}}
{{--                                                <a href="#"><i class="ti ti-linkedin"></i></a>--}}
{{--                                            </div>--}}
                                            <div class="comments-notes clearfix">
{{--                                                <div class="product-rating clearfix">--}}
{{--                                                    <ul class="star-rating clearfix">--}}
{{--                                                        @for($i = 0; $i < $rating; $i++)--}}
{{--                                                            <li class="fa fa-star"></li>--}}
{{--                                                        @endfor--}}
{{--                                                        @for($i = 0; $i < 5 - $rating; $i++)--}}
{{--                                                            <li class="fa fa-star-o"></li>--}}
{{--                                                        @endfor--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
                                            </div>
{{--                                            <div class="product_in-stock"><i class="fa fa-check-circle"></i><span> in Stock Only 14 left</span></div>--}}
                                            <span class="price">
                                                <ins>
                                                    <span class="product-Price-amount">
                                                        {{ $product->price }}<span class="product-Price-currencySymbol">₽</span><span> за метр</span>
                                                    </span>
                                                </ins>
{{--                                                <del>--}}
{{--                                                    <span class="product-Price-amount">--}}
{{--                                                        <span class="product-Price-currencySymbol">$</span>123.00--}}
{{--                                                    </span>--}}
{{--                                                </del>--}}
                                            </span>
                                            <div class="product-details__short-description">
                                                {{ $product->short_description }}
                                            </div>
                                            <div class="mt-15 mb-25">
                                                <div class="quantity">
                                                    <label>Колличество: </label>
                                                    <input type="text" value="1" name="quantity-number" class="qty">
                                                    <span class="inc quantity-button">+</span>
                                                    <span class="dec quantity-button">-</span>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <div class="add-to-cart">
                                                    <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor" id="pd_add_to_cart" data-id="{{ $product->id }}" href="#">Добавить в корзину</a>
                                                </div>
                                            </div>

{{--                                            <div id="block-reassurance-1" class="block-reassurance">--}}
{{--                                                <ul>--}}
{{--                                                    <li>--}}
{{--                                                        <div class="block-reassurance-item">--}}
{{--                                                            <i class="fa fa-lock"></i>--}}
{{--                                                            <span>Security policy (edit with Customer reassurance module)</span>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <div class="block-reassurance-item">--}}
{{--                                                            <i class="fa fa-truck"></i>--}}
{{--                                                            <span>Delivery policy (edit with Customer reassurance module)</span>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <div class="block-reassurance-item">--}}
{{--                                                            <i class="fa fa-arrows-h"></i>--}}
{{--                                                            <span>Return policy (edit with Customer reassurance module)</span>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
{{--                            <div class="pt-30 pb-60 res-991-pt-0 res-991-pb-30">--}}
{{--                                <div class="row no-gutters ttm-bgcolor-grey border">--}}
{{--                                    <div class="col-lg-4 col-md-4 col-sm-12">--}}
{{--                                        <!-- featured-icon-box -->--}}
{{--                                        <div class="featured-icon-box style3 text-center">--}}
{{--                                            <div class="ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-size-md">--}}
{{--                                                <i class="themifyicon ti-truck"></i>--}}
{{--                                            </div>--}}
{{--                                            <div class="featured-content">--}}
{{--                                                <div class="featured-title">--}}
{{--                                                    <h5>Fast & Free Shopping</h5>--}}
{{--                                                </div>--}}
{{--                                                <div class="featured-desc">--}}
{{--                                                    <p>All Order Over $199</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div><!-- featured-icon-box end-->--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-4 col-md-4 col-sm-12">--}}
{{--                                        <!-- featured-icon-box -->--}}
{{--                                        <div class="featured-icon-box style3 text-center">--}}
{{--                                            <div class="ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-size-md">--}}
{{--                                                <i class="themifyicon ti-reload"></i>--}}
{{--                                            </div>--}}
{{--                                            <div class="featured-content">--}}
{{--                                                <div class="featured-title">--}}
{{--                                                    <h5>100% Money Back Guaranty</h5>--}}
{{--                                                </div>--}}
{{--                                                <div class="featured-desc">--}}
{{--                                                    <p>30 Days Money Return</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div><!-- featured-icon-box end-->--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-4 col-md-4 col-sm-12">--}}
{{--                                        <!-- featured-icon-box -->--}}
{{--                                        <div class="featured-icon-box style3 text-center">--}}
{{--                                            <div class="ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-size-md">--}}
{{--                                                <i class="themifyicon ti-comments"></i>--}}
{{--                                            </div>--}}
{{--                                            <div class="featured-content">--}}
{{--                                                <div class="featured-title">--}}
{{--                                                    <h5>Support 24/7 Days</h5>--}}
{{--                                                </div>--}}
{{--                                                <div class="featured-desc">--}}
{{--                                                    <p>Hot Line: +123 456 789</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div><!-- featured-icon-box end-->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="ttm-tabs tabs-for-single-products" data-effect="fadeIn">
                                <ul class="tabs clearfix">
                                    <li class="tab active"><a href="#">Описание</a></li>
                                    <li class="tab"><a href="#">Характеристики</a></li>
{{--                                    <li class="tab"><a href="#">Отзывы</a></li>--}}
                                </ul>
                                <div class="content-tab">
                                    <!-- content-inner -->
                                    <div class="content-inner">
                                        {!! $product->content !!}
                                    </div><!-- content-inner end-->
                                    <!-- content-inner -->
                                    <div class="content-inner">
                                        @if(count($product->attributes) > 0)

                                            @foreach($product->attributes as $attribute)

                                                <div class="attr-line">
                                                    <div class="attr-name">
                                                        {{ $attribute->name }}:
                                                    </div>
                                                    <div class="attr-value">
                                                        <span class="value">{{ $attribute->pivot->value }}</span>
                                                        <span class="measure">{{ $attribute->measure }}</span>
                                                    </div>
                                                </div>


                                            @endforeach

                                        @endif
                                    </div><!-- content-inner end-->
                                    <!-- content-inner -->
                                    <div class="content-inner">
                                        <div id="reviews" class="woocommerce-Reviews">
                                            @include('frontend.productdetail.comments')
{{--                                            @include('frontend.productdetail.reviewform')--}}
                                        </div>
                                    </div><!-- content-inner -->

                                </div>
                            </div>
                        </div>
                        @include('frontend.productdetail.relatedproducts')
                    </div>
                </div><!-- row end -->
            </div>
        </section>
        <!-- single-product-section end -->

@endsection
