@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')

    <!--site-main start-->
    <div class="site-main">

        <!-- cart-section -->
        <section class="cart-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    @if(isset($cart))
                    <div class="col-lg-12 cart-page">
                        <!-- cart_table -->
                        <table class="table cart_table shop_table_responsive">
                            <thead>
                            <tr>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name">Товар</th>
                                <th class="product-price">Цена</th>
                                <th class="product-quantity">Колличество</th>
                                <th class="product-subtotal">Итого</th>
                                <th class="product-remove">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart as $item)
                            <tr class="cart_item">
                                <td class="product-thumbnail">
                                    <a href="{{ $item['url'] }}">
                                        <img class="img-fluid" src="{{ asset($item['photo']) }}" alt="product-img">
                                    </a>
                                </td>
                                <td class="product-name" data-title="Товар">
                                    <a href="{{ $item['url'] }}">{{ $item['name'] }}</a>
                                    <span>{{ $item['short_description'] }}</span>
                                </td>
                                <td class="product-price" data-title="Цена">
                                        <span class="Price-amount">
                                            {{ $item['price'] }}<span class="Price-currencySymbol">₽</span>
                                        </span>
                                </td>
                                <td class="product-quantity" data-title="Количество">
                                    <div class="quantity">
                                        <input type="number" value="{{ $item['quantity'] }}" data-id="{{ $item['id'] }}" name="quantity-number" class="qty"  size="4" min="1">
                                        <span class="inc quantity-button">+</span>
                                        <span class="dec quantity-button">-</span>
                                    </div>
                                </td>
                                <td class="product-subtotal" data-title="Итого">
                                        <span class="Price-amount">
                                            {{ $item['price'] * $item['quantity'] }}<span class="Price-currencySymbol">₽</span>
                                        </span>
                                </td>
                                <td class="product-remove">
                                    <a href="#" data-id="{{ $item['id'] }}" class="remove remove-cart_item">×</a>
                                </td>
                            </tr>
                            @endforeach

                            <tr>
                                <td colspan="6" class="actions">
                                    <div class="coupon">
                                        <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-icon-btn-left ttm-btn-color-skincolor" href="{{ URL::previous() }}"><i class="ti ti-arrow-left"></i>Назад</a>
                                    </div>
                                    <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-icon-btn-left ttm-btn-color-skincolor" href="#" id="clear_cart_btn"><i class="ti ti-close"></i>Очистить</a>
                                    <a href="/cart/checkout" class="checkout-button button">Заказать</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

{{--                    <div class="col-lg-12">--}}
{{--                        <!-- cart-collaterals -->--}}
{{--                        <div class="cart-collaterals">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-4">--}}
{{--                                    <div class="cart_shipping mt-30">--}}
{{--                                        <h5>Calculate Shipping<span class="ti ti-angle-down"></span></h5>--}}
{{--                                        <p class="text-input orderby">--}}
{{--                                            <select><option>United Kingdom (UK)</option>--}}
{{--                                                <option>Other</option>--}}
{{--                                            </select>--}}
{{--                                        </p>--}}
{{--                                        <p class="text-input"><input type="text" class="input-text zip-code" name="shipping_name" placeholder="Postal Code / Zip"></p>--}}
{{--                                        <div class="pt-20">--}}
{{--                                            <p class="text-input"><input type="button" class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor" name="update_cart" value="update total"></p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-4">--}}
{{--                                    <div class="cart_discount mt-30">--}}
{{--                                        <h5>Coupon Discount<span class="ti ti-angle-down"></span></h5>--}}
{{--                                        <p class="pt-10">Enter Your Coupon Code If You Have Done.</p>--}}
{{--                                        <p class="text-input"><input type="text" class="input-text zip-code" name="shipping_name" placeholder="Coupon Code"></p>--}}
{{--                                        <div class="pt-20">--}}
{{--                                            <p class="text-input"><input type="button" class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor" name="update_cart" value="Apply Coupon"></p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-4">--}}
{{--                                    <div class="cart_totals res-767-mt-30">--}}
{{--                                        <h5>Cart totals<span>$1450.00</span></h5>--}}
{{--                                        <h5>Shipping</h5>--}}
{{--                                        <p class="text-input">--}}
{{--                                            <input type="radio" name="grpShipping" value="Male" Checked>Standard<span>+ $10.00</span>--}}
{{--                                        </p>--}}
{{--                                        <p class="text-input">--}}
{{--                                            <input type="radio" name="grpShipping" value="Female">Standard<span>+ $10.00</span>--}}
{{--                                        </p>--}}
{{--                                        <h5>Total<span>$1460.00</span></h5>--}}
{{--                                    </div>--}}
{{--                                    <div class="proceed-to-checkout">--}}
{{--                                        <a href="#" class="checkout-button button">Proceed to checkout</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div><!-- cart-collaterals end-->--}}
{{--                    </div>--}}
                    @else
                    <div class="col-lg-12">
                        <p>Корзина пуста</p>
                    </div>
                    @endif
                </div>
            </div>
        </section><!-- cart-section end-->

    </div><!--site-main end-->

@endsection
