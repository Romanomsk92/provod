@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')


    <!--site-main start-->
    <div class="site-main">

        <!-- checkout-section -->
        <section class="checkout-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">

                    @if($cart > 0)
                    <div class="col-lg-12 cart-page">
                        <form name="checkout" method="post" class="checkout row" id="placeoreder" enctype="multipart/form-data" action="/placeoreder">
                            <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <div class="form-row">
                                        <label>Ф.И.О.</label>
                                        <input type="text" class="input-tex" name="name" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row">
                                        <label>Контактный телефон</label>
                                        <input type="tel" class="input-tex" name="phone" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row">
                                        <label>Email</label>
                                        <input type="email" class="input-tex" name="Email" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row" id="order_comments_field">
                                        <label>Адрес доставки:</label>
                                        <textarea name="delivery" class="input-text " id="delivery" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row" id="order_comments_field">
                                        <label>Комментарии</label>
                                        <textarea name="comment" class="input-text " id="comment" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-row" id="order_comments_field">
                                        <div class="add-file" style="cursor: pointer">
                                            <svg version="1.1" id="Capa_1" x="0px" y="0px"
                                                 width="25px" height="25px" viewBox="0 0 792 792" style="enable-background:new 0 0 792 792;" xml:space="preserve">
                                                <g>
                                                    <path d="M306,150.48v459.36c0,0-6.696,96.408,91.476,96.408C486,706.248,486,609.84,486,609.84V126.72C486,126.72,486,0,360,0
                                                        S234,126.72,234,126.72v483.12c0,0,0,182.16,162,182.16s162-182.16,162-182.16V126.72c0-19.8-36-19.8-36,0v483.12
                                                        c0,0,13.104,146.16-126,146.16c-126,0-126-146.16-126-146.16V126.72c0,0,0-90.72,90-90.72s90,90.72,90,90.72v483.12
                                                        c0,0,0,56.809-52.524,56.809c-52.523,0-55.476-56.809-55.476-56.809V150.48C342,130.68,306,130.68,306,150.48z"/>
                                                </g>
                                            </svg>
                                            <label style="cursor: pointer">Прикрепить реквизиты</label>
                                        </div>
                                        <input type="file" name="document" style="display: none">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="checkbox">
                                        <label data-target="#collapseOne">
                                            <input name="pickup" value="1" type="checkbox"/> <span class="ttm-textcolor-dark">Отметьте если хотите самостоятельно забрать товар с нашего склада.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="pt-30 res-991-pt-15">
                                    <div class="content-sec-head-style">
                                        <div class="content-area-sec-title">
                                            <h5>Ваш заказ</h5>
                                        </div>
                                    </div>
                                    <div id="order_review" class="checkout-review-order">
                                        <table class="cart_table checkout-review-order-table">
                                            <thead>
                                            <tr>
                                                <th class="product-name">Товар</th>
                                                <th class="product-total">Итого</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cart as $item)
                                                <tr class="cart_item">
                                                    <td class="product-name">{{ $item['name'] }}&nbsp;
                                                        <strong class="product-quantity">× {{ $item['quantity'] }}</strong>
                                                    </td>
                                                    <td class="product-total">
                                                        <span class="Price-amount">
                                                            {{ $item['price'] * $item['quantity'] }}<span class="Price-currencySymbol">₽</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>

                                            <tr class="order-total">
                                                <th>Итого</th>
                                                <td>
                                                    <strong>
                                                        <span class="woocommerce-Price-amount amount">
                                                            {{ $cart_summ }}<span class="woocommerce-Price-currencySymbol">₽</span>
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9"></div>
                            <div class="col-lg-3">
                                <div class="text-right place-order mt-30">
                                    <button type="submit" class="button" name="checkout_place_order" id="place_order" value="Заказать" data-value="Заказать">Заказать</button>
                                </div>
                            </div>
                        </form>
                    </div>
                        @endif
                </div>
            </div>
        </section>
        <!-- checkout-section end -->

    </div><!--site-main end-->
@endsection
