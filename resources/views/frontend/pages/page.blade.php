@extends('frontend.layout')

@section('page')

    @include('frontend.parts.breadcrumb')

    <div class="container">
{{--        <div class="row">--}}
            <div>
                {!! $page->content !!}
            </div>
{{--        </div>--}}
    </div>

@endsection
