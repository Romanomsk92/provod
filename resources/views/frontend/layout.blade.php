<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('frontend.parts.htmlhead')
@include('frontend.parts.styles')

<body class="@if(isset($bodyClass)) {{ $bodyClass }} @endif">

@include('frontend.parts.header')

<div class="page">

    @yield('page')

    <!--back-to-top start-->
    <a id="totop" href="#top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!--back-to-top end-->

</div>

@include('frontend.parts.footer')
@include('frontend.parts.cityselector')
@include('frontend.parts.scripts')
</body>
</html>
