jQuery(document).ready(function ($) {


    $('.add-file').click(function () {
        $(this).siblings('input[type="file"]').trigger('click');
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').delegate('.quantity-number', 'change', function () {
        // console.log($(this).val());
    });

    $('.add-to-cart-btn, #pd_add_to_cart').click(function (e) {
        e.preventDefault();
        var product_id = $(this).data('id');
        var url = "/addtocart";
        var quantity = 1;
        var btn = $(this);

        if($('[name="quantity-number"]').val()){
            quantity = $('[name="quantity-number"]').val();
        }

        $.ajax({

            type: "POST",
            url: url,
            data: { product_id: product_id, quantity: quantity},
            success: function (data) {
                $('.cart.dropdown').replaceWith(data);
                addCartMessage('Товар добавлен в корзину');
                $(btn).parents('.add-to-cart').addClass('incart');
                $(btn).find('span').html('В корзине');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('.remove-cart_item').click(function (e) {
        e.preventDefault();
        var product_id = $(this).data('id');
        var url = "/removefromcart";

        $(this).parents('.cart_item').remove();

        $.ajax({

            type: "POST",
            url: url,
            data: { product_id: product_id},
            success: function (data) {
                $('.cart.dropdown').replaceWith(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    $('#clear_cart_btn').click(function (e) {
        e.preventDefault();

        var url = "/clearcart";

        $.ajax({

            type: "POST",
            url: url,
            data: { },
            success: function (data) {
                let html = '    <div class="col-lg-12">\n' +
                    '               <p>Корзина пуста</p>\n' +
                    '           </div>';

                $('.cart.dropdown').replaceWith(data);
                $('.cart-page').replaceWith(html);
                popup_message('Корзина очищена');

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('#commentform').submit(function (e) {
        e.preventDefault();

        var url = "/addComment";

        $.ajax({
            type: "POST",
            url: url,
            data: $(this).serialize(),
            success: function (data) {
                $('.comment-respond').html('<p>Отзыв отправлен на модерацию</p>');
                popup_message('Отзыв добавлен');

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('#placeoreder').submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        var url = "/placeoreder";

        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                let html = '    <div class="col-lg-12">\n' +
                    '               <p>Заказ оформлен</p>\n' +
                    '           </div>';

                $('.cart.dropdown').replaceWith(data);
                $('.cart-page').replaceWith(html);
                popup_message('Заказ оформлен');

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('.ajax-form').submit(function (e) {
        e.preventDefault();

        let block = $(this),
            url = $(this).attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: $(this).serialize(),
            beforeSend: function() {
                $(block).addClass('loader');
            },
            complete: function() {
                $(block).removeClass('loader');
            },
            success: function (data) {
                let html = '<div class="form-content__header" style="text-align: center">\n' +
                    '                            Сообщение успешно отправлено\n' +
                    '                        </div>';

                $(block).html(html);
            }
        })
    });

    $('body').delegate('#category_filter select', 'change', function (e) {
        ajaxFilter($('#category_filter'));
    });

    rerollSliders();

    $('#category_filter').submit(function (e) {
        e.preventDefault();
        ajaxFilter($('#category_filter'));
    });
    //
    // function changeFilter() {
    //     ajaxFilter();
    // }

    function ajaxFilter(block) {

        var url = "/categoryFilter";

        var arr   = [];

        var reset = 0;

        for (let i = 0; i < $(block).serializeArray().length; i++){
            let param = $(block).serializeArray()[i],
                name  = param.name,
                val   = param.value,
                data  = 'crunch';

            if(name.indexOf('min') == 0){
                data = 'min';
            }

            if(name.indexOf('max') == 0){
                data = 'max';
            }

            if(data == "crunch"){
                arr.push({name: name, value: val})
            }

            if(parseInt($('input[name="' + name + '"]').data(data)) != parseInt(val)){
                if(data == 'max'){
                    if(reset == 0){
                        let name  = $(block).serializeArray()[i - 1].name,
                            val   = $(block).serializeArray()[i - 1].value;
                        arr.push({name: name, value: val});
                    }else{
                        reset = 0;
                    }

                }

                arr.push({name: name, value: val});

                if(data == 'min'){
                    let name  = $(block).serializeArray()[i + 1].name,
                        val   = $(block).serializeArray()[i + 1].value;
                    arr.push({name: name, value: val});

                    reset = 1;
                }
            }

        }

        var dataToSend = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: dataToSend,
            beforeSend: function() {
                $('.site-main').addClass('loader');
            },
            complete: function() {
                $('.site-main').removeClass('loader');
            },
            success: function (data) {
                var cat_url = $('.products-fitter').data('url');
                    history.pushState(null, null, cat_url + '/' + data['link']);
                    $('.products-fitter.products .products-shop').replaceWith(data['html']);
                    $('.widget.widget-price-filter').replaceWith(data['filter']);
                    rerollSliders();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    function rerollSliders() {
        $("#slider-range").slider({
            range: true,
            orientation: "horizontal",
            min: $('#min_price').data('min'),
            max: $('#max_price').data('max'),
            values: [$('#min_price').data('min'), $('#max_price').data('max')],

            slide: function (event, ui) {
                if ( ui.values[0] + "₽" == ui.values[1] + "₽") {
                    return false;
                }

                $("#min_price").val(ui.values[0] + "₽");
                $("#max_price").val(ui.values[1] + "₽");
            },
            change: function( event, ui ) {
                if(event.clientX > 0){
                    ajaxFilter($('#category_filter'));
                }
            }
        });

        $("#min_price").val($("#slider-range").slider("values", 0) + "₽");
        $("#max_price").val($("#slider-range").slider("values", 1) + "₽");

        $('.minimal-naya-rabochaya-temperatura_wrapper .slide-minimal-naya-rabochaya-temperatura').slider({
            range: true,
            orientation: "horizontal",
            min: $('.minimal-naya-rabochaya-temperatura_wrapper .min_value').data('min'),
            max: $('.minimal-naya-rabochaya-temperatura_wrapper .max_value').data('max'),
            values: [$('.minimal-naya-rabochaya-temperatura_wrapper .min_value').data('min'), $('.minimal-naya-rabochaya-temperatura_wrapper .max_value').data('max')],
            slide: function (event, ui) {
                if ( ui.values[0] == ui.values[1]) {
                    return false;
                }

                $('.minimal-naya-rabochaya-temperatura_wrapper').find(".min_value").val(ui.values[0]);
                $('.minimal-naya-rabochaya-temperatura_wrapper').find(".max_value").val(ui.values[1]);
            },
            change: function( event, ui ) {
                if(event.clientX > 0){
                    ajaxFilter($('#category_filter'));
                }
            }
        });

        $('.maksimal-naya-rabochaya-temperatura_wrapper .slide-maksimal-naya-rabochaya-temperatura').slider({
            range: true,
            orientation: "horizontal",
            min: $('.maksimal-naya-rabochaya-temperatura_wrapper .min_value').data('min'),
            max: $('.maksimal-naya-rabochaya-temperatura_wrapper .max_value').data('max'),
            values: [$('.maksimal-naya-rabochaya-temperatura_wrapper .min_value').data('min'), $('.maksimal-naya-rabochaya-temperatura_wrapper .max_value').data('max')],
            slide: function (event, ui) {
                if ( ui.values[0] == ui.values[1]) {
                    return false;
                }

                $('.maksimal-naya-rabochaya-temperatura_wrapper').find(".min_value").val(ui.values[0]);
                $('.maksimal-naya-rabochaya-temperatura_wrapper').find(".max_value").val(ui.values[1]);
            },
            change: function( event, ui ) {
                if(event.clientX > 0){
                    ajaxFilter($('#category_filter'));
                }
            }
        });
    }

    function popup_message(message)
    {
        Swal.fire(message);
    }

    function addCartMessage(message)
    {
        Swal.fire({
            title: message,
            showCancelButton: true,
            confirmButtonText: 'Перейти в корзину',
            cancelButtonText: 'Продолжить покупки',
        }).then((result) => {
            if (result.value == true) {
                window.location.href = '/cart';
            }
        });
    }

    $("input[type='tel']").mask("+7 (999) 999-9999");

    if($('#map').length > 0){
        ymaps.ready(init);

        function init(){
            var myMap = new ymaps.Map("map", {
                    center: [55.83239356890529,49.0511675],
                    zoom: 13
                }),
                myGeoObject = new ymaps.GeoObject({
                    // Описание геометрии.
                    geometry: {
                        type: "Point",
                        coordinates: [55.83239356890529,49.0511675]
                    }
                });
            myMap.geoObjects
                .add(myGeoObject)
                .add(new ymaps.Placemark([55.83239356890529,49.0511675], {
                    balloonContent: 'г. Казань ул. Восстания, д. 104'
                }, {
                    preset: 'islands#icon',
                    iconColor: '#fe4157'
                }))

        }
    }



});
