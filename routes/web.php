<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'PageController@index')->name('index');

Route::get('/kontakti', 'PageController@page')->name('kontakti');
Route::get('/o-kompanii', 'PageController@page')->name('o-kompanii');
Route::get('/dostavka-i-oplata', 'PageController@page')->name('dostavka-i-oplata');
Route::get('/garantii', 'PageController@page')->name('garantii');

Route::get('/search', 'PageController@search')->name('search');

Route::get('/cart', 'CartController@index')->name('cart');
Route::get('/cart/checkout', 'CartController@checkout')->name('checkout');
Route::post('/addtocart', 'CartController@addtocart')->name('addtocart');
Route::post('/clearcart', 'CartController@clearcart')->name('clearcart');
Route::post('/removefromcart', 'CartController@removefromcart')->name('removefromcart');
Route::post('/itemquantity', 'CartController@itemquantity')->name('itemquantity');
Route::post('/placeoreder', 'CartController@placeoreder')->name('placeoreder');

Route::post('/formsend', 'PageController@formsend')->name('formsend');

Route::post('/categoryFilter', 'ProductsController@categoryFilter')->name('categoryFilter');
Route::post('/addComment', 'ProductsController@addComment')->name('addComment');

if (!strpos(Request::url(),"admin") && !strpos(Request::url(),"assets")) {
    Route::get('/{category}/filter', 'ProductsController@filter')->name('filter')->where('category', '(.*)');
    Route::get('/{category}', 'ProductsController@category')->name('category')->where('category', '(.*)');
}




