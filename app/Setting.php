<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public function ptitle($product)
    {
        $val = $this->value;

        $val = str_replace('{product_name}', $product->name, $val);
        $val = str_replace('{product_price}', $product->price, $val);

        $parts = explode(' | ', $val);

        if(isset($parts[1])){
            $chars = explode(', ', $parts[1]);
        }

        $resistance = $product->attributes()->where('alias', 'soprotivlenie')->first();

        $ndeametr = $product->attributes()->where('alias', 'naruzhnyy-diametr-kabelya')->first();
        $nomdeametr = $product->attributes()->where('alias', 'nominal-nyy-diametr')->first();

        if($resistance && isset($chars[0])){
            $chars[0] = str_replace('{product_resistance}', $resistance->pivot->value, $chars[0]);
        }else {
            if(isset($chars[0])){
                unset($chars[0]);
            }
        }

        if($ndeametr && isset($chars[1])){
            $chars[1] = str_replace('{product_diameter}', $ndeametr->pivot->value , $chars[1]);
        }elseif($nomdeametr && isset($chars[1])){
            $chars[1] = str_replace('{product_diameter}', $nomdeametr->pivot->value , $chars[1]);
        }else{
            if(isset($chars[1])){
                unset($chars[1]);
            }
        }

        if(isset($chars[2])){
            unset($chars[2]);
        }

        if(isset($chars) && $chars){
            $val = $parts[0]. ' | '.implode(', ', $chars);
        }else{
            $val = $parts[0];
        }

        if(isset($parts[2])){
            $val = $val.' '.$parts[2];
        }

        return $val;
    }

    public function ctitle($category)
    {
        $val = $this->value;

        $val = str_replace('{category_name}', $category->name, $val);

        return $val;
    }

    public function filteredTitle($category, $params) {
        $val = $this->value;
        $val = str_replace('{category_name}', $category->name, $val);
        $parts = explode(' | ', $val);

        foreach ($params as $param) {
            $param = explode('__', $param);
            $filter[$param[0]] = $param[1];
        }
        foreach ($parts as $part) {
            $temp = [];

            switch ($part):
                case '{filter_mark}':
                    array_key_exists('markirovka', $filter) ? $title[] = 'Маркировка '.$filter['markirovka']: '';
                    break;
                case '{filter_dlina}':
                    array_key_exists('dlina', $filter) ? $title[] = 'Длина '.$filter['dlina'].' метров': '';
                    break;
                case '{filter_resistance}':
                    array_key_exists('soprotivlenie', $filter) ? $title[] = 'Сопротивление '.$filter['soprotivlenie'].' Ом': '';
                    break;
                case '{filter_weight}':
                    array_key_exists('ves', $filter) ? $title[] = 'Вес '.$filter['ves'].' кг': '';
                    break;
                case '{filter_min_temp}':
                    $temp[] = 'Минимальная температура ';
                    array_key_exists('min_minimal-naya-rabochaya-temperatura', $filter) ? $temp[] = 'от '.$filter['min_minimal-naya-rabochaya-temperatura']: '';
                    array_key_exists('max_minimal-naya-rabochaya-temperatura', $filter) ? $temp[] = 'до '.$filter['max_minimal-naya-rabochaya-temperatura']: '';
                    break;
                case '{filter_max_temp}':
                    $temp[] = 'Максимальная температура ';
                    array_key_exists('min_maksimal-naya-rabochaya-temperatura', $filter) ? $temp[] = 'от '.$filter['min_maksimal-naya-rabochaya-temperatura']: '';
                    array_key_exists('max_maksimal-naya-rabochaya-temperatura', $filter) ? $temp[] = 'до '.$filter['max_maksimal-naya-rabochaya-temperatura']: '';
                    break;
                case '{filter_d}':
                    array_key_exists('naruzhnyy-diametr-kabelya', $filter) ? $title[] = 'Диаметро '.$filter['naruzhnyy-diametr-kabelya']: '';
                    break;
                default:
                    $title[] = $part;
                break;
            endswitch;

            if (count($temp) > 1) : $title[] = implode(' ', $temp); endif;

        }

        return implode(' ', $title);
    }
}
