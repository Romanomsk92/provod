<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Category extends Model
{
    protected $table = 'categories';

    public function product()
    {
        return $this->belongsToMany(Product::class, 'category_products');
    }

    public function children(){
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent(){
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function getAllProducts($category_id = null, $products = null, $data = null)
    {
        if ($products === null && $category_id === null) {
            $products = collect();
        }

        if ($category_id === null) {
            $category = $this;
        }else{
            $category = Category::find($category_id);
        }

        if($category->product->count() > 0){
            $productsTemp = $category->product->where('active', 1);

            $productsTemp = Product::whereHas('category', function ($query) use ($category) {
                $query->where('alias', $category->alias);
            })->where('active', 1);

            if(isset($data) && isset($data['filteredBy'])) {
                foreach ($data['filteredBy'] as $fk => $fd) {
                    $productsTemp = $productsTemp->whereHas('attributes', function ($query) use ($fk, $fd) {
                         $query->where('attribute_id', $fk)
                            ->whereIn('value', $fd);
                    });
                }

            }

            $productsTemp = $productsTemp->get();
            $products = $products->merge($productsTemp);
        }

        if($category->children->count() > 0){
            foreach ($category->children as $item){
                $products = self::getAllProducts($item->id, $products, $data);
            }
        }

        $products = $products->unique('id');

        return $products;
    }


    public function getUrlAttribute()
    {
        $category_path = $this->getCategoryPath();

        if(is_array($category_path)){
            return '/'.implode('/', $category_path);
        }else{
            return '/'.$category_path;
        }
    }

    public function getCategoryPath($payload = null)
    {
        $cat = $this;

        $path[] = $cat->alias;
        if($cat->parent_id != 0){
            $cat = Category::where('id', $cat->parent_id)->first();
            $path[] = $cat->alias;
        }
        if($cat->parent_id != 0){
            $cat = Category::where('id', $cat->parent_id)->first();
            $path[] = $cat->alias;
        }

        return array_reverse($path);
    }

    public function getBreadcrumbAttribute($cat = null)
    {

        if($cat == null){
            $cat = $this;
        }

        if($cat->parent !== null){
            $breadcrumb = self::getBreadcrumbAttribute($cat->parent);
        }

        $data['alias'] = $cat->alias;
        $data['name'] = $cat->name;
        $data['url'] = $cat->url;

        $breadcrumb[] = $data;

        return $breadcrumb;
    }
}
