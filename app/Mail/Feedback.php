<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Feedback extends Mailable
{
    use Queueable, SerializesModels;

    public $feedback;
    public $subj;
    public $file;
    public $post = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($feedback, $subject, $post, $file)
    {
        $this->feedback = $feedback;
        $this->subj = $subject;
        $this->post = $post;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        unset($this->post['_token']);

        $mail = $this->subject($this->subj)->view('includes.mail');

        if(isset($this->file) && $this->file){
            $mail->attach($this->file->getRealPath(), [
                'as' => $this->file->getClientOriginalName(),
                'mime' => $this->file->getMimeType(),
            ]);
        }

        return $mail;
    }
}
