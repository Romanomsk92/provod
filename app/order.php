<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    public function product()
    {
        return $this->belongsToMany(Product::class, 'orders_product');
    }
}
