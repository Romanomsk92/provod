<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $id = $row[0];
        $price = $row[2];

        $product = Product::find($id);

        $product->price = $price;

        $product->save;

        return $product;
    }
}
