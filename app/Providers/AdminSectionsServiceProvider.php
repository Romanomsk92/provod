<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => 'App\Http\Sections\Users',
        \App\Attribute::class => 'App\Http\Sections\Attributes',
        \App\Category::class => 'App\Http\Sections\Categories',
        \App\Product::class => 'App\Http\Sections\Producto',
        \App\Page::class => 'App\Http\Sections\Pages',
        \App\Setting::class => 'App\Http\Sections\Setings',
//        \App\Testimonial::class => 'App\Http\Sections\Testimonials',
        \App\Review::class => 'App\Http\Sections\Reviews',
        \App\order::class => 'App\Http\Sections\Orders',
        \App\Slider::class => 'App\Http\Sections\Sliders',
        \App\City::class => 'App\Http\Sections\Cities',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
