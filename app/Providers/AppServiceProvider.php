<?php

namespace App\Providers;

use App\Form\AttributeField;
use App\Page;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \AdminFormElement::add('AttributeField', AttributeField::class);
    }
}
