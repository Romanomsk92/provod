<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;
use App\Category;

/**
 * Class Categories
 *
 * @property Category $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Categories extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
//        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-book');
    }

    public function getTitle()
    {
        return 'Категории';
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        return AdminDisplay::tree()->setValue('name')->setParentField('parent_id')->setOrderField('order');
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {

        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('name', 'Название')->required(),
                AdminFormElement::text('alias', 'Alias')->required()->addValidationRule('regex:/^[a-zA-Z0-9_-]+$/', 'Alias не должен содержать пробелы и кириллицу'),
                AdminFormElement::text('order', 'Сортировка'),
                AdminFormElement::select('parent_id', 'Родитльская категория', Category::class)->setUsageKey('id')->setDisplay('name')->exclude($id),
                AdminFormElement::text('meta_title', 'Meta Title'),
                AdminFormElement::text('meta_description', 'Meta description'),
                AdminFormElement::text('meta_keywords', 'Meta keywords'),
                AdminFormElement::text('header', 'H1'),


            ], 'col-xs-12 col-sm-6 col-md-6 col-lg-6')->addColumn([
                AdminFormElement::checkbox('active', 'Активен')->setDefaultValue(1),
                AdminFormElement::image('photo', 'Изображние'),

            ], 'col-xs-12 col-sm-6 col-md-6 col-lg-6')->addColumn([
                AdminFormElement::wysiwyg('content_before', 'Контент до'),
                AdminFormElement::wysiwyg('content_after', 'Контент после'),

            ], 'col-xs-12 col-sm-6 col-md-12 col-lg-12'),
        ]);


        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
