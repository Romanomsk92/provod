<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Category;
use App\Form\AttributeField;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Producto
 *
 * @property \App\Product $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Producto extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
//        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

    public function getTitle()
    {
        return 'Товары';
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
       $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::image('photo', 'Фото'),
            AdminColumn::link('name', 'Название')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name', 'like', '%'.$search.'%')
                    ;
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('created_at', $direction);
                })
            ,
            AdminColumn::text('alias', 'Алиас'),
            AdminColumn::boolean('active', 'Включен'),
            AdminColumn::text('created_at', 'Дата', 'updated_at')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('updated_at', $direction);
                })
                ->setSearchable(false)
            ,
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        $display->setColumnFilters([
            AdminColumnFilter::select()
                ->setModelForOptions(\App\Product::class, 'name')
                ->setLoadOptionsQueryPreparer(function($element, $query) {
                    return $query;
                })
                ->setDisplay('name')
                ->setColumnName('name')
                ->setPlaceholder('All names')
            ,
        ]);
        $display->getColumnFilters()->setPlacement('card.heading');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('name', 'Название')->required(),
                AdminFormElement::text('alias', 'Alias')->required()->addValidationRule('regex:/^[a-zA-Z0-9_-]+$/'),
                AdminFormElement::text('sku', 'Артикул'),
                AdminFormElement::text('price', 'Цена'),
                AdminFormElement::number('warehouse', 'Склад'),
                AdminFormElement::multiselect('category', 'Категория', Category::class)->setUsageKey('id')->setDisplay('name'),
                AdminFormElement::text('meta_title', 'Meta Title'),
                AdminFormElement::text('meta_description', 'Meta description'),
                AdminFormElement::text('meta_keywords', 'Meta keywords'),
                AdminFormElement::text('header', 'H1'),


            ], 'col-xs-12 col-sm-6 col-md-6 col-lg-6')->addColumn([
                AdminFormElement::checkbox('active', 'Активен')->setDefaultValue(1),
                AdminFormElement::checkbox('hit', 'Хит')->setDefaultValue(1),
                AdminFormElement::checkbox('stock', 'Акция')->setDefaultValue(1),
                AdminFormElement::checkbox('under_order', 'Под заказ')->setDefaultValue(1),
                AdminFormElement::image('photo', 'Изображние'),
                AdminFormElement::image('draw', 'Чертеж'),

            ], 'col-xs-12 col-sm-6 col-md-6 col-lg-6')->addColumn([
                AdminFormElement::text('short_description', 'Краткое описание'),
                AdminFormElement::wysiwyg('content', 'Описание'),

            ], 'col-xs-12 col-sm-6 col-md-6 col-lg-6')->addColumn([
                AdminFormElement::AttributeField('attributes', 'Аттрибут', [
                    AdminFormElement::text('value', 'Значение'),
                ])->setRelatedElementDisplayName('name')->setLabel('Характеристики'),

            ], 'col-xs-12 col-sm-6 col-md-6 col-lg-6'),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
