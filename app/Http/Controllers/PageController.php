<?php

namespace App\Http\Controllers;

use App\Mail\Feedback;
use Illuminate\Support\Facades\Mail;
use App\Category;
use App\Page;
use App\Product;
use App\Review;
use App\Slider;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{

    public function index()
    {
        $hit = Product::where(['hit' => 1, 'active' => 1])->limit(8)->get();
        $stock = Product::where(['stock' => 1, 'active' => 1])->limit(8)->get();
        $testimonials = Review::where('moderate', 1)->limit(8)->get();

        $page = Page::where('alias', '/')->get()->first();

        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;
        $meta_keywords = $page->meta_keywords;

        $slides = Slider::where('active', 1)->get();

        return view('frontend.pages.mainpage', compact('hit', 'stock', 'testimonials', 'slides', 'meta_title', 'meta_description', 'meta_keywords'));
    }

    public function page(Request $request)
    {
      
        $alias = Route::currentRouteName();

        $page = Page::where('alias', $alias)->get()->first();

        $breadcrumb = $page->Breadcrumb;

        $page->header ? $header = $page->header : $page->name;

        $meta_title = $page->meta_title;
        $meta_description = $page->meta_description;
        $meta_keywords = $page->meta_keywords;

        $page->template ? $template = 'frontend.pages.'.$page->template : $template = 'frontend.pages.page';

        return view($template, compact('breadcrumb', 'header', 'page', 'meta_title', 'meta_description', 'meta_keywords'));
    }

    public function search(Request $request)
    {
        if(!$request->search){
            return redirect(url()->previous());
        }

        $breadcrumb[0]['alias'] = 'search';
        $breadcrumb[0]['name'] = 'Поиск';

        $header = 'Результаты поиска';

        $products = Product::where('name', 'like', '%'.$request->search.'%')->where('active', 1)->get();

        return View('frontend.pages.search', compact('products', 'breadcrumb', 'header'));
    }

     public function formsend(Request $request)
    {
        $post = $request->post();

        $comment = 'Это сообщение отправлено из формы обратной связи';

        $toEmail = "info@pro-vod.ru";
        $subject = "Письмо с сайта pro-vod.ru";

        Mail::to($toEmail)
            ->send(new Feedback($comment, $subject, $post));

        return true;

    }
}
