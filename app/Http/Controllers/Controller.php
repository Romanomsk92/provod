<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Contact;
use App\Page;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // Страницы
        $pages = Page::orderBy('order', 'asc')->get();
        view()->share('pages', $pages);

        // Категории
        $categories_all = Category::all();
        view()->share('categories_all', $categories_all);

        $categories = Category::where(['active' => 1])->where('parent_id', NULL)->orWhere('parent_id', 0)->get();
        view()->share('categories', $categories);

        $cities = City::where(['active' => 1])->orderBy('title', 'ASC')->get();

        $list = [];
        foreach ($cities as $c){
            $fw = mb_substr($c->title, 0, 1);
            $list[$fw][] = $c;
        }

        view()->share('cities', $list);

        $this->middleware(function ($request, $next) {

            $cart = session()->get('cart');
            view()->share('cart', $cart);

            $uri = explode('.', Request::getHost())[0];

            if($uri == 'www'){
                $uri = explode('.', Request::getHost())[1];
            }

            if ($uri != explode('.', $_SERVER['APP_URL'])[0]){
                $city = City::where('slug', $uri)->first();
            }
            elseif(session()->get('city')){
                $city = City::where('id', session()->get('city'))->first();
            }else{
                $city = City::where('default', 1)->first();
            }
            view()->share('city', $city);

            $summ = 0;
            $quantity = 0;
            if(isset($cart)){
                foreach ($cart as $item){
                    $summ = $summ + ($item['quantity'] * $item['price']);
                    $quantity = $quantity + $item['quantity'];
                }
            }
            view()->share('cart_summ', $summ);
            view()->share('cart_quantity', $quantity);

            return $next($request);
        });
    }
}
