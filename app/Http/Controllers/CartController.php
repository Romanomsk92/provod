<?php

namespace App\Http\Controllers;

use App\Mail\Feedback;
use Illuminate\Support\Facades\Mail;
use App\order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CartController extends Controller
{
    public function index()
    {
        $breadcrumb[0]['alias'] = 'cart';
        $breadcrumb[0]['name'] = 'Корзина';

        return View('frontend.pages.cart', compact('breadcrumb'));
    }

    public function checkout()
    {
        $breadcrumb[0]['alias'] = 'cart';
        $breadcrumb[0]['name'] = 'Корзина';
        $breadcrumb[1]['alias'] = 'checkout';
        $breadcrumb[1]['name'] = 'Оформление заказа';

        return View('frontend.pages.checkout', compact('breadcrumb'));
    }

    public function clearcart()
    {
        $cart = null;

        session()->forget('cart');

        $cart_summ = 0;
        $cart_quantity = 0;
        if(isset($cart)){
            foreach ($cart as $item){
                $cart_summ = $cart_summ + ($item['quantity'] * $item['price']);
                $cart_quantity = $cart_quantity + $item['quantity'];
            }
        }

        return View('frontend.parts.cart', compact('cart','cart_quantity','cart_summ'));
    }

    public function placeoreder(Request $request)
    {
        $cart = session()->get('cart');


        $order = new order();
        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->email = $request->Email;
        $order->address = $request->delivery;
        $order->comment = $request->comment;
        $order->pickup = $request->pickup ? 1 : 0;

        $order->save();

        $post = $request->post();

        foreach ($cart as $item){
            $product = Product::find($item['id']);
            $post['products'][] = "<a href='". url('/') . $item['url'] ."'>" . $item['name'] . " - ". $item['quantity'] ." шт</a>";
            $order->product()->attach($product);
        }
        $comment = 'Оформлен новый заказ';
        $toEmail = "info@pro-vod.ru";
        $subject = "Новый заказ на сайте pro-vod.ru";
        $file = $request->file('document');
        Mail::to($toEmail)
            ->send(new Feedback($comment, $subject, $post, $file));

        return $this->clearcart();

    }

    public function removefromcart(Request $request)
    {
        $cart = session()->get('cart');

        unset($cart[$request->product_id]);

        session()->forget('cart');

        session()->put('cart', $cart);

        $cart_summ = 0;
        $cart_quantity = 0;
        if(isset($cart)){
            foreach ($cart as $item){
                $cart_summ = $cart_summ + ($item['quantity'] * $item['price']);
                $cart_quantity = $cart_quantity + $item['quantity'];
            }
        }

        return View('frontend.parts.cart', compact('cart','cart_quantity','cart_summ'));
    }

    public function itemquantity(Request $request){
        $cart = session()->get('cart');

        if(isset($cart[$request->product_id])){
            $cart[$request->product_id]['quantity'] = $request->quantity;
        }

        session()->forget('cart');

        session()->put('cart', $cart);

        $cart_summ = 0;
        $cart_quantity = 0;
        if(isset($cart)){
            foreach ($cart as $item){
                $cart_summ = $cart_summ + ($item['quantity'] * $item['price']);
                $cart_quantity = $cart_quantity + $item['quantity'];
            }
        }

        return View('frontend.parts.cart', compact('cart','cart_quantity','cart_summ'));
    }

    public function addtocart(Request $request)
    {
        $id = $request->post('product_id');
        $quantity = $request->post('quantity');

        $product = Product::find($id);

        if(!$product) {

            return false;

        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                $id => [
                    "id" => $product->id,
                    "name" => $product->name,
                    "short_description" => $product->short_description,
                    "quantity" => $quantity,
                    "price" => $product->price,
                    "photo" => $product->photo,
                    "url" => $product->Url
                ]
            ];


        }

        // if cart not empty then check if this product exist then increment quantity
        elseif(isset($cart[$id])) {

            $cart[$id]['quantity'] = $cart[$id]['quantity'] + $quantity;


        }else{
            // if item not exist in cart then add to cart with quantity = 1
            $cart[$id] = [
                "id" => $product->id,
                "name" => $product->name,
                "short_description" => $product->short_description,
                "quantity" => $quantity,
                "price" => $product->price,
                "photo" => $product->photo,
                "url" => $product->Url
            ];

        }

        session()->put('cart', $cart);


        $cart_summ = 0;
        $cart_quantity = 0;
        if(isset($cart)){
            foreach ($cart as $item){
                $cart_summ = $cart_summ + ($item['quantity'] * $item['price']);
                $cart_quantity = $cart_quantity + $item['quantity'];
            }
        }

        return View('frontend.parts.cart', compact('cart','cart_quantity','cart_summ'));
    }
}
