<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Category;
use App\Imports\ProductsImport;
use App\Product;
use App\Review;
use App\Setting;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProductsExport;
use Illuminate\Database\Eloquent\Builder;
use AdminSection;

class ProductsController extends Controller
{
    public function category(Request $request)
    {
        $alias = $request->category;

        $path = explode('/', $alias);

        if ($category = Category::where('alias', last($path))->get()->first()) {

            $products = $category->getAllProducts()->unique('id');

            $category->header ? $header = $category->header : $category->name;
            $breadcrumb = $category->Breadcrumb;

            $ccheck = '/' . implode('/', array_slice(explode('/', $request->url()), 3));
            $bcheck = end($breadcrumb)['url'];
            if ($bcheck != $ccheck){
                return redirect($bcheck, 301);
            }

            $scategories = $category->children;

            if (!$category->meta_title) {
                $meta_title = Setting::where('key', 'category_meta_title')->first()->ctitle($category);
            } else {
                $meta_title = $category->meta_title;
            }
            if (!$category->meta_description) {
                $meta_description = Setting::where('key', 'category_meta_description')->first()->ctitle($category);
            } else {
                $meta_description = $category->meta_description;
            }

            $data = $this->filterData($products);

            $attributes = $data['attributes'];
            $products = $data['products'];

            return view('frontend.pages.category', compact('breadcrumb', 'category', 'products', 'header', 'meta_title', 'meta_description', 'attributes', 'scategories'));
        }

        if ($product = Product::where('alias', last($path))->where('active', 1)->get()->first()) {

            $breadcrumb = $product->Breadcrumb;

            $ccheck = '/' . implode('/', array_slice(explode('/', $request->url()), 3));
            $bcheck = end($breadcrumb)['url'];
            if ($bcheck != $ccheck){
                return redirect($bcheck, 301);
            }

            $reviews = $product->Reviews;
            $rating = $product->Rating;
            $topsellers = Product::getRelatedProducts($product);
            $product->header ? $header = $product->header : $product->name;

            if (!$product->meta_title) {
                $meta_title = Setting::where('key', 'product_meta_title')->first()->ptitle($product);
            } else {
                $meta_title = $product->meta_title;
            }
            if (!$product->meta_description) {
                $meta_description = Setting::where('key', 'product_meta_description')->first()->ptitle($product);
            } else {
                $meta_description = $product->meta_description;
            }

            return view('frontend.pages.productdetail', compact('breadcrumb', 'product', 'reviews', 'rating', 'topsellers', 'header', 'meta_title', 'meta_description'));
        }

        return response(view('frontend.pages.error'), 404);
    }

    public function addComment(Request $request)
    {
        if (!$request->pid and !$request->author) {
            return false;
        }

        $review = new Review();
        $product = Product::find($request->pid);

        $review->name = $request->author;
        $review->rating = $request->rating;
        $review->email = $request->email;
        $review->comment = $request->comment;
        $review->save();

        $review->product()->attach($product);

        return true;
    }

    public function filter(Request $request)
    {
        $post = [];

        $alias = $request->category;

        $path = explode('/', $alias);

        $params = explode('--', array_pop($path));

        $category = Category::where('alias', last($path))->get()->first();

        $meta_title = Setting::where('key', 'filter_meta_title')->first()->filteredTitle($category, $params);
        $meta_description = Setting::where('key', 'filter_meta_desc')->first()->filteredTitle($category, $params);

        count($params) > 2 ? $meta = '<meta name="robots" content="noindex, nofollow" />' : $meta = '';

        foreach ($params as $k=>$param){
            $post[explode('__', $param)[0]] = explode('__', $param)[1];
        }

        $data = $this->getFilterBy($post);

        if($data['index'] > 3){
            $meta = '<meta name="robots" content="noindex">';
        }

        $header = $category->name;

        $breadcrumb = $category->Breadcrumb;

        $products = $category->getAllProducts(null, null, $data);

        $data = $this->filterData($products, $data);

        $attributes = $data['attributes'];
        $products = $data['products'];

        return view('frontend.pages.category', compact('breadcrumb', 'category', 'products', 'header', 'meta_title', 'meta_description', 'attributes', 'meta', 'post'));
    }


    public function categoryFilter(Request $request)
    {
        $post = [];
        $link = [];

        foreach ($request->all() as $i){
            $post[$i['name']] = $i['value'];
        }

        $category = Category::where('id', $post['category'])->get()->first();

        $data = $this->getFilterBy($post);

        $products = $category->getAllProducts(null, null, $data);

        $data = $this->filterData($products, $data);

        $attributes = $data['attributes'];
        $products = $data['products'];

        $data['html'] = view('frontend.category.productsAjax', compact('products'))->render();
        $data['filter'] = view('frontend.category.filter', compact('products', 'attributes', 'category', 'post'))->render();

        return $data;
    }

    function getFilterBy($post) {
        $attributes = Attribute::all();
        $data['index'] = 0;

        foreach ($post as $k => $item) {
            if ($k == 'category' || $item === null) {
                continue;
            }

            $data['index']++;

            switch ($k):
                case 'min_price':
                    $link[] = $k . '__' . intval($item);
                    $data['min_price'] = intval($item);
                    break;
                case 'max_price':
                    $link[] = $k . '__' . intval($item);
                    $data['max_price'] = intval($item);
                    break;
                case 'min_minimal-naya-rabochaya-temperatura':
                case 'max_minimal-naya-rabochaya-temperatura':
                case 'min_maksimal-naya-rabochaya-temperatura':
                case 'max_maksimal-naya-rabochaya-temperatura':

                    $link[] = $k . '__' . $item;
                    $attr = $attributes->firstWhere('alias', explode('_', $k)[1]);
                    if(isset($attr->id)){
                        $data['filteredBy'][$attr->id][] = $item;
                    }
                    break;
                default:
                    $link[] = $k . '__' . $item;
                    $attr = $attributes->firstWhere('alias', $k);

                    if(isset($attr->id)){
                        $data['filteredBy'][$attr->id][] = $item;
                    }
                    break;
            endswitch;
        }

        if(isset($data['filteredBy']) || isset($data['min_price']) || $data['max_price']){
            $data['link'] = implode('--', $link) . '/filter';

        }else {
            $data['link'] = implode('--', $link) . '';
        }

        return $data;
    }

    function filterData($products, $data = null) {

        if(isset($data['min_price'])){
            $products = $products->where('price', '>=', intval($data['min_price']));
        }

        if(isset($data['max_price'])){
            $products = $products->where('price', '<=', intval($data['max_price']));
        }

        $products_array = $products->pluck('id')->toArray();

        $attributes = Attribute::whereHas('product', function (Builder $query) use ($products_array) {
            $query->whereIn('product_id', $products_array);
        })->get()->keyBy('id')->toArray();

        foreach ($products as $value) {
            foreach ($value->attributes as $att) {
                unset($val);
                preg_match('/\d+?(?=.|$|,)/', $att->pivot->value, $intKey);

                switch ($att->alias) {
                    case 'naruzhnyy-diametr-kabelya':
                        $temp = explode('±', $att->pivot->value);
                        preg_match('/(\d)+?(\.|\,)?(\d)/isU', $temp[0], $val);
                        if (!isset($val[0])) {
                            $val = preg_replace('/(\D)/isU', '', $att->pivot->value);
                        } else {
                            $val = preg_replace('/(\D)/isU', '', $val[0]);
                        }
                        break;
                    case 'soprotivlenie':
                        $val = preg_replace('/(\D)/isU', '', $att->pivot->value);
                        $val = str_pad($val, 5, '0');
                        break;
                    case 'dlina':
                    case 'minimal-naya-rabochaya-temperatura':
                    case 'maksimal-naya-rabochaya-temperatura':
                    case 'markirovka':
                    case 'standart':
                        $val = preg_replace('/(\D)/isU', '', $att->pivot->value);
                        break;
                    case 'ves':
                        $val = str_replace(',', '.', $att->pivot->value);
                        break;
                }

                if (isset($val)) {
                    if (!isset($attributes[$att->id]['data'])) {
                        $attributes[$att->id]['data'][$val] = $att->pivot->value;
                        continue;
                    }

                    if (array_search($val, $attributes[$att->id]['data']) === false) {
                        $attributes[$att->id]['data'][$val] = $att->pivot->value;
                    }

                    ksort($attributes[$att->id]['data']);
                } else {
                    if (!isset($attributes[$att->id]['data'])) {
                        $attributes[$att->id]['data'][] = $att->pivot->value;
                        continue;
                    }

                    if (array_search($att->pivot->value, $attributes[$att->id]['data']) === false) {
                        $attributes[$att->id]['data'][] = $att->pivot->value;
                    }
                }

            }

        }

        foreach ($products as $key => $product){
            foreach ($product->attributes as $attribute){
                if(isset($data['filteredBy']) && array_key_exists($attribute->id, $data['filteredBy'])){
                    if(count($data['filteredBy'][$attribute->id]) > 1){
                        if($data['filteredBy'][$attribute->id][0] > preg_replace('/(\+)/', '', intval($attribute->pivot->value)) || $data['filteredBy'][$attribute->id][1] < preg_replace('/(\+)/', '', intval($attribute->pivot->value))) $products->forget($key);
                    }else{
                        if($data['filteredBy'][$attribute->id][0] != $attribute->pivot->value) $products->forget($key);
                    }
                }

            }
        }

        $data['attributes'] = $attributes;
        $data['products'] = $products;

        return $data;
    }

    public function export()
    {
        return Excel::download(new ProductsExport(), 'products.xlsx');
    }

    public function import(Request $request)
    {
        if($request->post()){
            $file = $request->file('import');
            $file->getClientOriginalName();
            $file->getClientOriginalExtension();
            $file->getRealPath();
            $file->getSize();
            $file->getMimeType();
            $destinationPath = 'uploads';

//            $file->move($destinationPath,$file->getClientOriginalName());

            Excel::import(new ProductsImport(), $file);

            return redirect('/admin')->with('success', 'All good!');
        }else {
            return AdminSection::view(view('includes.import'));
        }

    }

}
