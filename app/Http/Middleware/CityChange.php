<?php

namespace App\Http\Middleware;

use App\City;
use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class CityChange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);


        $uri = explode('.', Request::getHost())[0];

        if(preg_match("/\.(txt|xml)$/", $uri)) {
            return $response;
        }
        if(preg_match("/^\/admin\//", $uri)) {
            return $response;
        }
        if ($response instanceof \Symfony\Component\HttpFoundation\BinaryFileResponse) {
            return $response;
        } else {
            try {
                $buffer = $response->getContent();

                if($response->getStatusCode() != 404){

                    if($uri == 'www'){
                        $uri = explode('.', Request::getHost())[1];
                    }

                    if ($uri != explode('.', $_SERVER['APP_URL'])[0]){
                        $city = City::where('slug', $uri)->first();
                    }
                    elseif(session()->get('city')){
                        $city = City::where('id', session()->get('city'))->first();
                    }else{
                        $city = City::where('default', 1)->first();
                    }

                    preg_match('/<title>(.*)<\/title>/', $buffer, $preg_title);

                    if(isset($preg_title[1])){
                        $buffer = preg_replace('/<title>(.*)<\/title>/', '<title>'.$preg_title[1].' - '.$city->title.'</title>', $buffer);
                    }
                    preg_match('/(<meta name="description" content=")(.*)(" \/>)/isU', $buffer, $preg_description);

                    if(isset($preg_description[2])) {
                        $buffer = preg_replace('/(<meta name="description" content=")(.*)(" \/>)/isU', '$1' . $preg_description[2] . ' - ' . $city->title . '$3', $buffer);
                    }

                }

                $response->setContent($buffer);
            } catch (\Exception $e) {
                dd($e);
            }

        }
        return $response;
    }
}
