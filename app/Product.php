<?php

namespace App;

use App\Attribute;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function category()
    {
        return $this->belongsToMany(Category::class, 'category_products');
    }

    public function order()
    {
        return $this->belongsToMany(order::class, 'orders_product');
    }

    public function review()
    {
        return $this->belongsToMany(Review::class, 'product_review');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'product_attribute')->withPivot('value');
    }

    public function getReviewsAttribute()
    {
        return $this->review()->where('moderate', 1)->get();
    }

    public function getRatingAttribute()
    {
        $rating = 0;

        if(count($this->Reviews) == 0){
            return 5;
        }

        foreach ($this->Reviews as $item){
            $rating = $rating + $item->rating;
        }

        $rating = $rating / count($this->Reviews);

        return $rating;
    }

    public function getUrlAttribute()
    {
        $category_path = $this->getCategoryPath();
        if(is_array($category_path)){
            $data = '/'.implode('/', $category_path).'/'.$this->alias;
            return $data;
        }else{
            return '/'.$category_path.'/'.$this->alias;
        }
    }

    public function getCategoryPath($payload = null)
    {
        $cat = $this->category->first();
        $path[] = $cat->alias;
        if($cat->parent_id != 0){
            $cat = Category::where('id', $cat->parent_id)->first();
            $path[] = $cat->alias;
        }
        if($cat->parent_id != 0){
            $cat = Category::where('id', $cat->parent_id)->first();
            $path[] = $cat->alias;
        }

        return array_reverse($path);
    }

    public function getBreadcrumbAttribute($cat = null, $payload = null)
    {

        if($cat == null){
            $cat = $this->category()->first();
        }

        if($cat->parent !== null){
            $breadcrumb = self::getBreadcrumbAttribute($cat->parent, $payload = 1);
            $payload = 0;
        }

        $data['alias'] = $cat->alias;
        $data['name'] = $cat->name;
        $data['url'] = $cat->url;

        $breadcrumb[] = $data;

        if($payload == null){
            $data['alias'] = $this->alias;
            $data['name'] = $this->name;
            $data['url'] = $this->url;

            $breadcrumb[] = $data;
        }

        return $breadcrumb;
    }

    public static function getRelatedProducts($product)
    {
        return self::where('topseller', 1)->where('id', '<>', $product->id)->inRandomOrder()->take(8)->get();
    }

}
