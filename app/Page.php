<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function getBreadcrumbAttribute()
    {
        $breadcrumb[0]['alias'] = $this->alias;
        $breadcrumb[0]['name'] = $this->name;
        return $breadcrumb;
    }
}
