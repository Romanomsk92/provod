<?php

use SleepingOwl\Admin\Navigation\Page;

return [
    [
        'title' => 'Dashboard',
        'icon'  => 'fas fa-tachometer-alt',
        'url'   => route('admin.dashboard'),
        'priority'   =>  1,
    ],
//    [
//        'title' =>  'Параметры',
//        'icon'  =>  'fas fa-user-cog',
//        'pages' =>  [
//            (new Page(\App\Tag::class))
//                ->setIcon('fa fa-code')
//                ->setPriority(0),
//        ],
//    ],
    [
        'title' => 'Продукция',
        'icon'  => 'fa fa-balance-scale',
        'pages' => [
            (new Page(\App\Category::class))
                ->setIcon('fa fa-book')
                ->setPriority(0),
            (new Page(\App\Product::class))
                ->setIcon('fa fa-shopping-cart')
                ->setPriority(0),
            (new Page(\App\Attribute::class))
                ->setIcon('fab fa-adversal')
                ->setPriority(0),
        ],
    ],
    [
        'title' => 'Настройки',
        'icon'  => 'fas fa-cogs',
        'pages' => [
            (new Page(\App\User::class))
                ->setIcon('fas fa-users')
                ->setPriority(0),
            (new Page(\App\Setting::class))
                ->setIcon('fas fa-cog')
                ->setPriority(0),
            [
                'title' => 'Импорт',
                'icon'  => '',
                'url'   => route('admin.import'),
                'priority'   =>  1,
            ],
            [
                'title' => 'Экспорт',
                'icon'  => '',
                'url'   => route('admin.export'),
                'priority'   =>  1,
            ],
        ],
    ],
];
