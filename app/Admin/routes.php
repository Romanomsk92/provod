<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = 'Define your dashboard here.';
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);

Route::get('/export',
    [
        'as' => 'admin.export',
        'uses' => '\App\Http\Controllers\ProductsController@export'
    ]
);

Route::match(['get', 'post'], '/import',
    [
        'as' => 'admin.import',
        'uses' => '\App\Http\Controllers\ProductsController@import'
    ]
);
